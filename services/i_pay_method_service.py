from abc import ABC, abstractmethod

from entities.user import User


class IPayMethodService(ABC):

    @abstractmethod
    def get_user_payment_data(self, user: User): pass

    @abstractmethod
    def create_customer(self, user: User): pass

    @abstractmethod
    def register_customer(self, user: User): pass

    @abstractmethod
    def get_register_status_customer(self, token: str): pass

    @abstractmethod
    def create_subscription(self, user: User, plan_id: int, start): pass

    @abstractmethod
    def get_subscription(self, subscription_id): pass
