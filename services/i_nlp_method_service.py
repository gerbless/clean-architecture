from abc import ABC, abstractmethod


class INlpMethodService(ABC):

    @abstractmethod
    def run_query(self, search_query: str, count: int, statistics: bool): pass
