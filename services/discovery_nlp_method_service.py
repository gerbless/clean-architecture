from injector import inject
from watson_developer_cloud import DiscoveryV1
from watson_developer_cloud import WatsonApiException

from services.discovery_nlp_format_query import DiscoveryNlpFormatQuery
from environment_config import IEnvironmentConfig
from services.exceptions import DiscoveryException
from services.i_nlp_method_service import INlpMethodService


class DiscoveryNlpMethodService(INlpMethodService):

    @inject
    def __init__(self, environment_config: IEnvironmentConfig):
        self.api_url = 'https://gateway.watsonplatform.net/discovery/api'
        self.username = environment_config.DISCOVERY["username"]
        self.password = environment_config.DISCOVERY["password"]
        self.environment = environment_config.DISCOVERY["environment_id"]
        self.collection = environment_config.DISCOVERY["collection_id"]
        self.version = environment_config.DISCOVERY["version"]

    def start_service(self):
        return DiscoveryV1(username=self.username,
                           password=self.password,
                           version=self.version,
                           url=self.api_url)

    def run_query(self, search_query: str, count: int, statistics: bool):

        kwargs = {'environment_id': self.environment,
                  'collection_id': self.collection}

        format_query = DiscoveryNlpFormatQuery(search_query, count)

        if statistics:
            query_params = format_query.get_query_for_documents_statistics()
        else:
            query_params = format_query.get_query_for_documents()

        kwargs.update(query_params)

        try:
            service = self.start_service()
            response = service.query(**kwargs)

            if response.get_status_code() == 200:
                return response.get_result()

        except WatsonApiException as ex:
            raise DiscoveryException(ex.code, ex.message)
