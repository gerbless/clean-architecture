from injector import Binder

from services.console_email_method_service import ConsoleEmailMethodService
from services.discovery_nlp_method_service import DiscoveryNlpMethodService
from services.flow_pay_method_service import FlowPayMethodService
from services.google_recaptcha_method_service import GoogleRecaptchaMethodService
from services.i_email_method_service import IEmailMethodService
from services.i_nlp_method_service import INlpMethodService
from services.i_pay_method_service import IPayMethodService
from services.i_recaptcha_method_service import IRecaptchaMethodService
from services.mailgun_email_method_service import MailgunEmailMethodService


def configure_services_binding(binder: Binder) -> Binder:
    binder.bind(IEmailMethodService, ConsoleEmailMethodService)
    binder.bind(IPayMethodService, FlowPayMethodService)
    binder.bind(INlpMethodService, DiscoveryNlpMethodService)
    binder.bind(IRecaptchaMethodService, GoogleRecaptchaMethodService)
    return binder
