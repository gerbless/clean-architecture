import hashlib
import hmac

import requests
from injector import inject
from typing import Dict

from entities.user import User
from entities.plan import Plan
from environment_config import IEnvironmentConfig
from services.exceptions import WrongCredentialsException, FlowException
from services.i_pay_method_service import IPayMethodService


class FlowPayMethodService(IPayMethodService):

    @inject
    def __init__(self, environment_config: IEnvironmentConfig):
        self.api_key = environment_config.FLOW["api_key"]
        self.url = "https://sandbox.flow.cl/api"
        self.secret_key = environment_config.FLOW["secret_key"]
        self.base_url = environment_config.BASE_URL

    def sign_parameters(self, params: Dict) -> str:
        string_param = ''
        sorted_keys = sorted(params.keys())
        for key in sorted_keys:
            string_param += key + "=" + str(params[key]) + '&'

        string_param = string_param[:-1]
        return hmac.new(self.secret_key, string_param.encode('utf-8'), hashlib.sha256).hexdigest()

    def create_customer(self, user: User):
        response = requests.post(
            self.url + "/customer/create",
            data={
                "apiKey": self.api_key,
                "name": user.full_name(),
                "email": user.email,
                "externalId": user.external_uuid,
                "s": self.sign_parameters({
                    "apiKey": self.api_key,
                    "name": user.full_name(),
                    "email": user.email,
                    "externalId": user.external_uuid,
                })
            }
        )
        return response.json().get("customerId")

    def register_customer(self, user: User):
        response = requests.post(
            self.url + "/customer/register",
            data={
                "apiKey": self.api_key,
                "customerId": user.customerId,
                "url_return": self.base_url + "/subscribe/",
                "s": self.sign_parameters({
                    "apiKey": self.api_key,
                    "customerId": user.customerId,
                    "url_return": self.base_url + "/subscribe/",
                })
            }
        )
        return response.json().get("url") + "?token=" + response.json().get("token")

    def get_register_status_customer(self, token: str) -> Dict:
        response = requests.get(
            self.url + "/customer/getRegisterStatus",
            params={
                "apiKey": self.api_key,
                "token": token,
                "s": self.sign_parameters({"apiKey": self.api_key, "token": token})
            }
        )

        json_response = response.json()
        if response.status_code == 200:
            return json_response

        raise FlowException(json_response['message'])

    def get_user_payment_data(self, user: User) -> Dict:
        response = requests.get(
            self.url + "/customer/get",
            params={
                "apiKey": self.api_key,
                "customerId": user.customerId,
                "s": self.sign_parameters({"apiKey": self.api_key, "customerId": user.customerId})
            }
        )
        json_response = response.json()
        if response.status_code == 200:
            return json_response

        if json_response['code'] == 109:
            raise WrongCredentialsException()

        raise FlowException(json_response['message'])

    def create_plan(self, plan: Plan):
        response = requests.post(
            self.url + "/plans/create",
            data={
                "apiKey": self.api_key,
                "planId": plan.id,
                "name": plan.name,
                "currency": plan.currency,
                "amount": plan.amount,
                "interval": plan.interval,
                "s": self.sign_parameters({
                    "apiKey": self.api_key,
                    "planId": plan.id,
                    "name": plan.name,
                    "currency": plan.currency,
                    "amount": plan.amount,
                    "interval": plan.interval
                })
            }
        )
        return response.json().get("planId")

    def get_plan(self, plan: Plan) -> Dict:
        response = requests.get(
            self.url + "/plans/get",
            params={
                "apiKey": self.api_key,
                "planId": plan.id,
                "s": self.sign_parameters({"apiKey": self.api_key, "planId": plan.id})
            }
        )

        json_response = response.json()
        if response.status_code == 200:
            return json_response

        raise FlowException(json_response['message'])

    def create_subscription(self, user: User, plan: Plan, start):
        response = requests.post(
            self.url + "/subscription/create",
            data={
                "apiKey": self.api_key,
                "planId": plan.id,
                "customerId": user.customerId,
                "subscription_start": start,
                "trial_period_days": 30,
                "s": self.sign_parameters({
                    "apiKey": self.api_key,
                    "planId": plan.id,
                    "customerId": user.customerId,
                    "subscription_start": start,
                    "trial_period_days": 30
                })
            }
        )
        return response.json().get("subscriptionId")

    def get_subscription(self, subscription_id) -> Dict:
        response = requests.get(
            self.url + "/subscription/get",
            params={
                "apiKey": self.api_key,
                "subscriptionId": subscription_id,
                "s": self.sign_parameters({"apiKey": self.api_key, "subscriptionId": subscription_id})
            }
        )

        json_response = response.json()
        if response.status_code == 200:
            return json_response

        raise FlowException(json_response['message'])


