import requests
from injector import inject

from environment_config import IEnvironmentConfig
from services.exceptions import MailgunException
from services.exceptions import SendEmailException
from services.i_email_method_service import IEmailMethodService


class MailgunEmailMethodService(IEmailMethodService):

    @inject
    def __init__(self, environment_config: IEnvironmentConfig):
        self.api_url = 'https://api.mailgun.net/v3/'
        self.domain = environment_config.MAILGUN["domain"]
        self.api_key = environment_config.MAILGUN["api_key"]

    def get_credentials(self):
        url = self.api_url + 'domains/' + self.domain + '/credentials'
        response = requests.get(url, auth=self.auth)

        if response.status_code == 200:
            return b'success'

        if response.status_code == 401:
            raise MailgunException()

    def send_message(self, data_message: dict):
        url = self.api_url + self.domain + '/messages'
        data = {
            "from": data_message['from'],
            "to": data_message['to'],
            "subject": data_message['subject'],
            "html": data_message['content']
        }

        response = requests.post(url, auth=self.auth, data=data)

        if response.status_code == 200:
            return True

        if response.status_code == 400:
            return b'parameters are missing'

        raise SendEmailException()

    @property
    def auth(self):
        return 'api', self.api_key
