import requests
from injector import inject

from environment_config import IEnvironmentConfig
from services.exceptions import RecaptchaException
from services.i_recaptcha_method_service import IRecaptchaMethodService


class GoogleRecaptchaMethodService(IRecaptchaMethodService):

    @inject
    def __init__(self, environment_config: IEnvironmentConfig):
        self.site_verify_url = "https://www.google.com/recaptcha/api/siteverify"
        self.secret_key = environment_config.RECAPTCHA["secret_key"]
        self.site_key = environment_config.RECAPTCHA["site_secret"]


    def check_human(self, captcha_response: str):
        data = {
            'secret': self.secret_key,
            'response': captcha_response
        }

        response = requests.post(self.site_verify_url, data)
        response_json = response.json()

        if response_json['success']:
            return True

        raise RecaptchaException


    def get_site_key(self):
        return self.site_key
