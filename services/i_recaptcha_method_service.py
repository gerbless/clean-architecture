from abc import ABC, abstractmethod


class IRecaptchaMethodService(ABC):

    @abstractmethod
    def check_human(self, captcha_response: str): pass

    @abstractmethod
    def get_site_key(self): pass