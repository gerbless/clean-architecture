import logging

from services.i_email_method_service import IEmailMethodService


class ConsoleEmailMethodService(IEmailMethodService):
    def get_credentials(self):
        pass

    def send_message(self, params: dict):
        print(params)
        return True
