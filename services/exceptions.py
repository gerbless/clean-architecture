class MailgunException(Exception): pass
class WrongCredentialsException(Exception): pass
class FlowException(Exception): pass
class SendEmailException(Exception): pass
class DiscoveryException(Exception): pass
class RecaptchaException(Exception): pass
