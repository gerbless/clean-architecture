from unittest.mock import MagicMock

import pytest

from environment_config import IEnvironmentConfig
from services.exceptions import MailgunException, SendEmailException
from services.mailgun_email_method_service import MailgunEmailMethodService


def test_mailgun_email_method_service_get_credentials_raise_error_if_api_key_domain_are_invalid():
    configuration: IEnvironmentConfig = MagicMock(IEnvironmentConfig)
    configuration.MAILGUN = {
        "api_key": "demo",
        "domain": "demos@tars.tecnology"
    }
    mailgun_service = MailgunEmailMethodService(configuration)

    with pytest.raises(MailgunException):
        mailgun_service.get_credentials()


def test_mailgun_email_service_send_message_failure_if_api_key_domain_are_invalid():
    configuration: IEnvironmentConfig = MagicMock(IEnvironmentConfig)
    configuration.MAILGUN = {
        "api_key": "demo",
        "domain": "demos@tars.tecnology"
    }
    mailgun_service = MailgunEmailMethodService(configuration)

    params = {
        "from": "admin@gmail.com",
        "to": "mirakel1401@gmail.com",
        "subject": "Demo Mailgun",
        "content": "Plaintext content"
    }
    with pytest.raises(SendEmailException):
        mailgun_service.send_message(params)
