from unittest.mock import MagicMock

import pytest

from environment_config import IEnvironmentConfig
from services.discovery_nlp_method_service import DiscoveryNlpMethodService
from services.exceptions import DiscoveryException


def test_discovery_nlp_method_service_run_query_should_raise_error_if_username_password_are_invalid():
    configuration: IEnvironmentConfig = MagicMock(IEnvironmentConfig)
    configuration.DISCOVERY = {
        "username": "admin",
        "password": "demo",
        "environment_id": "system",
        "collection_id": "news-es",
        "version": "2018-08-01",
    }

    discovery_service = DiscoveryNlpMethodService(configuration)
    with pytest.raises(DiscoveryException) as ex:
        discovery_service.run_query("Top musical November", 20, False)

    assert '401' in str(ex.value)


def test_discovery_nlp_method_service_run_query_should_raise_error_if_environment_and_collection_do_not_exist():
    configuration: IEnvironmentConfig = MagicMock(IEnvironmentConfig)
    configuration.DISCOVERY = {
        "username": "471bbf8c-ad60-4f00-9090-c0a52847df3b",
        "password": "XNJmVKnAqaYT",
        "environment_id": "new-environment",
        "collection_id": "new-collection",
        "version": "2018-08-01",
    }

    discovery_service = DiscoveryNlpMethodService(configuration)

    with pytest.raises(DiscoveryException) as ex:
        discovery_service.run_query("Top musical November", 20, False)

    assert '400' in str(ex.value)
