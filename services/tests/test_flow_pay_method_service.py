from unittest.mock import MagicMock

import pytest

from entities.plan import Plan
from entities.user import User
from environment_config import IEnvironmentConfig, EnvironmentConfig
from services.exceptions import WrongCredentialsException, FlowException
from services.flow_pay_method_service import FlowPayMethodService


def test_flow_pay_method_service_get_user_payment_data_should_raise_error_if_credentials_are_invalid():
    configuration: IEnvironmentConfig = MagicMock(IEnvironmentConfig)
    configuration.FLOW = {
        "api_key": "",
        "secret_key": b'11111'
    }
    configuration.BASE_URL = "http://localhost:5000"
    flow_service = FlowPayMethodService(configuration)
    with pytest.raises(WrongCredentialsException):
        flow_service.get_user_payment_data(MagicMock())


def test_flow_pay_method_service_sign_params_should_encrypt_all_parameters():
    configuration: IEnvironmentConfig = MagicMock(IEnvironmentConfig)
    configuration.FLOW = {
        "api_key": "",
        "secret_key": b'11111'
    }
    configuration.BASE_URL = "http://localhost:5000"
    flow_service = FlowPayMethodService(configuration)
    signed_params = flow_service.sign_parameters({'b': 1234, 'a': "1234", 'c': "Nombre completo"})
    assert signed_params == 'bb948a12ad3a7422247032846854a96cf0d269e4a5debce9d9c9ad714b96f37d'


def test_flow_pay_method_service_get_user_payment_data_should_raise_error_if_client_id_do_not_exist():
    configuration = EnvironmentConfig()
    flow_service = FlowPayMethodService(configuration)
    user: User = MagicMock(User)
    user.customer_id = "asdasda"
    with pytest.raises(FlowException):
        flow_service.get_user_payment_data(user)


def test_flow_pay_method_service_create_customer_should_return_a_customer_id():
    configuration = EnvironmentConfig()
    flow_service = FlowPayMethodService(configuration)
    user: User = MagicMock(User)
    user.email = "alvaro@tars.technology"
    user.id = 2
    user.full_name.return_value = "Alvaro Servio"
    user.customer_id = flow_service.create_customer(user)
    # assert user.customer_id is not None


def test_flow_pay_method_service_get_register_status_customer_return_raise_error_if_token_invalid():
    configuration = EnvironmentConfig()
    flow_service = FlowPayMethodService(configuration)
    with pytest.raises(FlowException):
        flow_service.get_register_status_customer("token_invalid")


def test_flow_pay_method_service_get_plan_return_none_if_plan_id_does_not_exist():
    configuration = EnvironmentConfig()
    flow_service = FlowPayMethodService(configuration)
    plan: Plan = MagicMock(Plan)
    plan.id = 5
    assert flow_service.get_plan(plan) is None


def test_flow_pay_method_service_get_subscription_return_raise_error_if_subscription_id_invalid():
    configuration = EnvironmentConfig()
    flow_service = FlowPayMethodService(configuration)
    with pytest.raises(FlowException):
        flow_service.get_subscription("subscriptionId")