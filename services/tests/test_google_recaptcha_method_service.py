from unittest.mock import MagicMock

import pytest

from environment_config import IEnvironmentConfig
from services.exceptions import RecaptchaException
from services.google_recaptcha_method_service import GoogleRecaptchaMethodService


def test_google_recaptcha_method_service_check_human_return_raise_error_if_captcha_response_invalid():
    configuration: IEnvironmentConfig = MagicMock(IEnvironmentConfig)
    configuration.RECAPTCHA = {
        "site_secret": "6LcAH30UAAAAABp3iWvPv5mvUHri0FyhTzHbcbIu",
        "secret_key": "6LcAH30UAAAAAJnOsWEhMIIl2Im_IA5PIG62486_"
    }

    recaptcha_service = GoogleRecaptchaMethodService(configuration)
    with pytest.raises(RecaptchaException):
        recaptcha_service.check_human("captcha-response")
