from services.console_email_method_service import ConsoleEmailMethodService


def test_console_email_method_service_get_credentials_should_not_fail():
    email_service = ConsoleEmailMethodService()
    email_service.get_credentials()


def test_mailgun_email_service_send_message_successfully():
    mail_service = ConsoleEmailMethodService()

    params = {
        "from": "Mailgun <smartbird@sandbox704de7888292474390d02fc278a4fa71.mailgun.org>",
        "to": "mirakel1401@gmail.com",
        "subject": "Demo Mailgun",
        "text": "Plaintext content"
    }
    response = mail_service.send_message(params)
    assert True == response