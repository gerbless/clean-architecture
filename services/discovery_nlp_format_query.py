class DiscoveryNlpFormatQuery():

    def __init__(self, text: str, count: int):
        self.text = text
        self.count = count

    def get_query_for_documents(self):
        params = {}
        params['highlight'] = True
        params['natural_language_query'] = self.text
        params['count'] = self.count
        params['passages'] = False
        params['passages_count'] = self.count
        params['deduplicate'] = True

        return params

    def get_query_for_documents_statistics(self):
        params = {}
        params['aggregation'] = '[' + ', '.join(['term(enriched_text.sentiment.document.label)',
                                                 'timeslice(field:crawl_date,interval:1day,time_zone:America/New_York,anomaly:true).term(enriched_text.keywords.text,count:1).term(title,count:1)',   
                                                 'filter(enriched_title.entities.type::Company).term(enriched_title.entities.text).timeslice(crawl_date,1day).term(enriched_text.sentiment.document.label)']) + \
                                ']'

        params.update(self.get_query_for_documents())

        return params
