from flask import session, redirect, url_for, flash


def guardian_login(func):
    def validate_login(*args, **kwargs):
        if session.get('user') is None:
            flash("Usted no se ha identificado.", "error")
            return redirect(url_for('login'))
        return func(*args, **kwargs)
    return validate_login
