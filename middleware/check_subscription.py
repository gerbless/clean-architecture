from flask import session, redirect, url_for


def check_subscription(func):
    def wrapper(*args):
        if not args[0].navigation_lock_use_case.verify_subscription(session.get('user')):
            if session.get('business_sector') is None:
                return redirect(url_for('sectors'))
            if session.get('plan') is None:
                return redirect(url_for('plans'))

        if not args[0].navigation_lock_use_case.verify_credit_card(session.get('user')):
            return redirect(url_for('lock'))

        return func(*args)
    return wrapper