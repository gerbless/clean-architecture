from abc import ABC, abstractmethod


class ISearchDocumentUseCase(ABC):

    @abstractmethod
    def get_documents_data(self, search_query: str, max_results: int, statistics: bool): pass
