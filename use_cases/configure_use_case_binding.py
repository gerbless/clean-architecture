from injector import Binder

from use_cases.i_register_use_case import IRegisterUseCase
from use_cases.i_login_use_case import ILoginUseCase
from use_cases.i_password_reset_use_case import IPasswordResetUseCase
from use_cases.i_preconfigured_questions_case import IPreconfiguredQuestionsUseCase
from use_cases.i_search_document_use_case import ISearchDocumentUseCase
from use_cases.i_subscription_use_case import ISubscriptionUseCase
from use_cases.i_password_edit_use_case import IPasswordEditUseCase
from use_cases.i_search_history_use_case import ISearchHistoryUseCase
from use_cases.i_verify_account_use_case import IVerifyAccountUseCase
from use_cases.i_resend_verify_account_use_case import IResendVerifyAccountUseCase
from use_cases.i_search_question_use_case import ISearchQuestionUseCase
from use_cases.i_business_sector_use_case import IBusinessSectorUseCase
from use_cases.i_navigation_lock_use_case import INavigationLockUseCase
from use_cases.i_plan_use_case import IPlanUseCase

from use_cases.register_use_case import RegisterUserCase
from use_cases.login_use_case import LoginUseCase
from use_cases.password_reset_use_case import PasswordResetUseCase
from use_cases.preconfigured_questions_case import PreconfiguredQuestionsUseCase
from use_cases.search_document_use_case import SearchDocumentUseCase
from use_cases.subscription_use_case import SubscriptionUseCase
from use_cases.password_edit_use_case import PasswordEditUseCase
from use_cases.search_history_use_case import SearchHistoryUseCase
from use_cases.verify_account_use_case import VerifyAccountUseCase
from use_cases.resend_verify_account_use_case import ResendVerifyAccountUserCase
from use_cases.search_question_use_case import SearchQuestionUseCase
from use_cases.business_sector_use_case import BusinessSectorUseCase
from use_cases.navigation_lock_use_case import NavigationLockUseCase
from use_cases.plan_use_case import PlanUseCase


def configure_user_case_binding(binder: Binder) -> Binder:
    binder.bind(IRegisterUseCase, RegisterUserCase)
    binder.bind(ILoginUseCase, LoginUseCase)
    binder.bind(IPasswordResetUseCase, PasswordResetUseCase)
    binder.bind(IPreconfiguredQuestionsUseCase, PreconfiguredQuestionsUseCase)
    binder.bind(ISubscriptionUseCase, SubscriptionUseCase)
    binder.bind(ISearchDocumentUseCase, SearchDocumentUseCase)
    binder.bind(IPasswordEditUseCase, PasswordEditUseCase)
    binder.bind(ISearchHistoryUseCase, SearchHistoryUseCase)
    binder.bind(IVerifyAccountUseCase, VerifyAccountUseCase)
    binder.bind(IResendVerifyAccountUseCase, ResendVerifyAccountUserCase)
    binder.bind(ISearchQuestionUseCase, SearchQuestionUseCase)
    binder.bind(IBusinessSectorUseCase, BusinessSectorUseCase)
    binder.bind(INavigationLockUseCase, NavigationLockUseCase)
    binder.bind(IPlanUseCase, PlanUseCase)
    return binder
