from abc import ABC, abstractmethod


class IBusinessSectorUseCase(ABC):

    @abstractmethod
    def get_business_sectors_by_user(self, email: str): pass

    @abstractmethod
    def get_business_sectors(self): pass

    @abstractmethod
    def index_business_sector(self, uid: int = None): pass
