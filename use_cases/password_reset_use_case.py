from flask import url_for
from injector import inject
from itsdangerous import URLSafeTimedSerializer

from environment_config import EnvironmentConfig
from repositories.i_user_repository import IUserRepository
from services.i_email_method_service import IEmailMethodService
from use_cases.exceptions import InvalidUserException
from use_cases.i_password_reset_use_case import IPasswordResetUseCase


class PasswordResetUseCase(IPasswordResetUseCase):

    @inject
    def __init__(self, user_repository: IUserRepository, email_method_service: IEmailMethodService):
        self.user_repository = user_repository
        self.email_method_service = email_method_service

    def generate_token(self, email: str):
        password_reset_serializer = URLSafeTimedSerializer(EnvironmentConfig.SECRET_KEY)
        return password_reset_serializer.dumps(email, salt='password-reset-salt')

    def generate_password_url(self, email: str):
        return url_for('password_edit', token=self.generate_token(email), _external=True)

    def send_message(self, email: str, content: str):
        user = self.user_repository.get_user_by_email(email)
        data_message = {}

        if user:
            data_message['from'] = 'Mailgun <smartbird@sandbox704de7888292474390d02fc278a4fa71.mailgun.org>'
            data_message['to'] = user.email
            data_message['subject'] = 'Restablece tu contraseña'
            data_message['content'] = content
            status = self.email_method_service.send_message(data_message)

            return status
        raise InvalidUserException()
