from injector import inject
from itsdangerous import URLSafeTimedSerializer, BadTimeSignature, SignatureExpired

from environment_config import EnvironmentConfig
from repositories.i_user_repository import IUserRepository
from use_cases.exceptions import InvalidUserException
from use_cases.i_verify_account_use_case import IVerifyAccountUseCase


class VerifyAccountUseCase(IVerifyAccountUseCase):

    @inject
    def __init__(self, user_repository: IUserRepository):
        self.user_repository = user_repository

    def verify_token(self, token: str, expiration: int = 3600):
        email_serializer = URLSafeTimedSerializer(EnvironmentConfig.SECRET_KEY)
        try:
            email = email_serializer.loads(token,
                                           salt='email-confirm-key',
                                           max_age=expiration)
        except SignatureExpired:
            return None
        except BadTimeSignature:
            return None
        return email

    def confirmation_email(self, email: str):
        user = self.user_repository.get_user_by_email(email)
        if user:
            user.active_email()
            self.user_repository.add(user)
            return True
        raise InvalidUserException()

    def is_confirmed_email(self, email: str):
        user = self.user_repository.get_user_by_email(email)
        if user:
            return user.is_active_email()
        raise InvalidUserException()