from flask import url_for
from injector import inject
from itsdangerous import URLSafeTimedSerializer

from environment_config import EnvironmentConfig
from repositories.i_user_repository import IUserRepository
from services.i_email_method_service import IEmailMethodService
from use_cases.exceptions import InvalidUserException
from use_cases.i_resend_verify_account_use_case import IResendVerifyAccountUseCase


class ResendVerifyAccountUserCase(IResendVerifyAccountUseCase):

    @inject
    def __init__(self, user_repository: IUserRepository, email_method_service: IEmailMethodService):
        self.user_repository = user_repository
        self.email_method_service = email_method_service

    def generate_token(self, email: str):
        email_serializer = URLSafeTimedSerializer(EnvironmentConfig.SECRET_KEY)
        return email_serializer.dumps(email, salt='email-confirm-key')

    def generate_verify_url(self, email: str):
        return url_for('verify_account', token=self.generate_token(email), _external=True)

    def send_message(self, email: str, content: str):
        user = self.user_repository.get_user_by_email(email)
        if user:
            data_message = {}
            data_message['from'] = 'Mailgun <smartbird@sandbox704de7888292474390d02fc278a4fa71.mailgun.org>'
            data_message['to'] = user.email
            data_message['subject'] = 'Verificación de cuenta'
            data_message['content'] = content
            status = self.email_method_service.send_message(data_message)
            return status
        raise InvalidUserException()