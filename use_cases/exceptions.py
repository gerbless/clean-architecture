from environment_config import EnvironmentConfig

class InvalidUserException(Exception):
    pass


class InvalidPasswordException(Exception):
    pass


class InvalidEmailException(Exception):
    pass


class InvalidSendMailException(Exception):
    pass


class InvalidQuestionsException(Exception):
    def __init__(self, message_user: str, err_debug: str):
        self.error = err_debug
        self.message = message_user
        if EnvironmentConfig.MODE_DEBBUGER:
            print(self.error)

    def __str__(self):
        return self.message


class InvalidBusinessSectorException(Exception):
    pass


class InvalidDocumentException(Exception):
    pass


class InvalidQuestionsUserException(Exception):
    pass


class InvalidFavoriteDocumentException(Exception):
    pass


class InvalidSearchHistoryException(Exception):
    pass


class InvalidSearchQuestionException(Exception):
    def __init__(self, message_user: str, err_debug: str):
        self.error = err_debug
        self.message = message_user
        if EnvironmentConfig.MODE_DEBBUGER:
            print(self.error)

    def __str__(self):
        return self.message


class InvalidSubscriptionException(Exception):
    pass


class InvalidPlanException(Exception):
    pass


class InvalidUBusinessSectorException(Exception):
    pass
