from injector import inject

from repositories.i_user_repository import IUserRepository
from use_cases.exceptions import InvalidUserException
from use_cases.i_navigation_lock_use_case import INavigationLockUseCase


class NavigationLockUseCase(INavigationLockUseCase):

    @inject
    def __init__(self, user_repository: IUserRepository):
        self.user_repository = user_repository

    def verify_credit_card(self, email: str):
        user = self.user_repository.get_user_by_email(email)
        if user:
            return user.is_credit_card()
        raise InvalidUserException()

    def verify_subscription(self, email: str):
        try:
            subscriptions = self.user_repository.get_subscriptions(email)
            if subscriptions:
                return True
        except InvalidUserException:
            pass
        return False


