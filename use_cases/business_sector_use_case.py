from injector import inject

from repositories.i_business_sector_repository import IBusinessSectorRepository
from repositories.i_user_repository import IUserRepository
from use_cases.i_business_sector_use_case import IBusinessSectorUseCase
from use_cases.exceptions import InvalidUserException


class BusinessSectorUseCase(IBusinessSectorUseCase):

    @inject
    def __init__(self, business_sector_repository: IBusinessSectorRepository, user_repository: IUserRepository):
        self.business_sector_repository = business_sector_repository
        self.user_repository = user_repository

    def get_business_sectors_by_user(self, email: str):
        try:
            subscriptions = self.user_repository.get_subscriptions(email)
            if subscriptions:
                business_sector = []
                for subscription in subscriptions:
                    business_sector.append(subscription.business_sector)
                return business_sector
        except InvalidUserException:
            pass
        return InvalidUserException()

    def get_business_sectors(self):
        return self.business_sector_repository.get_all()

    def index_business_sector(self, uid: int = None):
        if uid is None:
            business_sector = self.business_sector_repository.get_all()
        else:
            business_sector = self.business_sector_repository.get_id(uid=uid)

        return business_sector
