from abc import ABC, abstractmethod

from entities.user import User


class IRegisterUseCase(ABC):

    @abstractmethod
    def register_user(self, name: str, last_name: str, email: str, password: str, confirm_email: bool): pass

    @abstractmethod
    def check_captcha(self, captcha: str): pass

    @abstractmethod
    def get_captcha_key(self): pass

    @abstractmethod
    def send_message(self, user: User, content: str): pass

    @abstractmethod
    def generate_verify_url(self, email): pass
