from abc import ABC, abstractmethod


class ISearchQuestionUseCase(ABC):

    @abstractmethod
    def get_search_question(self, search: str): pass
