from datetime import datetime

from injector import inject

from entities.subscription import Subscription
from repositories.i_plan_repository import IPlanRepository
from repositories.i_subscription_repository import ISubscriptionRepository
from repositories.i_user_repository import IUserRepository
from services.exceptions import FlowException
from services.i_pay_method_service import IPayMethodService
from use_cases.exceptions import InvalidUserException, InvalidSubscriptionException
from use_cases.i_subscription_use_case import ISubscriptionUseCase


class SubscriptionUseCase(ISubscriptionUseCase):

    @inject
    def __init__(self, user_repository: IUserRepository, subscription_repository: ISubscriptionRepository,
                 plan_repository: IPlanRepository, pay_method_service: IPayMethodService):
        self.user_repository = user_repository
        self.subscription_repository = subscription_repository
        self.plan_repository = plan_repository
        self.pay_method_service = pay_method_service

    def get_subscription_data(self, email: str):
        user = self.user_repository.get_user_by_email(email)
        user_data = self.pay_method_service.get_user_payment_data(user)

        if user_data["creditCardType"] is not None:
            subscription = {
                "credit_cards": [
                    {
                        "creditCardType": user_data["creditCardType"],
                        "last4CardDigits": user_data["last4CardDigits"]
                    }
                ]
            }
        else:
            subscription = {
                "credit_cards": []
            }

        subscription["new_credit_card_url"] = self.register_customer(email)

        return subscription

    def add_customer(self, email: str):
        user = self.user_repository.get_user_by_email(email)
        if user:
            customer_id = self.pay_method_service.create_customer(user)
            user.update_customer(customer_id)
            self.user_repository.add(user)
            return True
        raise InvalidUserException()

    def register_customer(self, email: str):
        user = self.user_repository.get_user_by_email(email)
        if user:
            result = self.pay_method_service.register_customer(user)
            return result
        raise InvalidUserException()

    def get_card_register_status(self, token: str):
        try:
            result = self.pay_method_service.get_register_status_customer(token)
            user = self.user_repository.get_user_by_customerId(result["customerId"])
            if user:
                user.register_credit_card()
                self.user_repository.add(user)
            return True
        except FlowException:
            pass
        return False

    def add_subscription(self, email: str, plan_id: int, sector_id: int):
        try:
            user = self.user_repository.get_user_by_email(email)
            plan = self.plan_repository.get_id(plan_id)
            subscriptionId = self.pay_method_service.create_subscription(user, plan, self.date_now())
            subscription = Subscription(user.id, sector_id, plan_id, subscriptionId)
            self.subscription_repository.add(subscription)
            return True
        except InvalidUserException:
            pass
        except InvalidSubscriptionException:
            pass
        return False

    def date_now(self):
        return datetime.now().strftime("%Y-%m-%d")
