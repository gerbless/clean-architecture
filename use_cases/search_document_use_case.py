from injector import inject

from services.exceptions import DiscoveryException
from services.i_nlp_method_service import INlpMethodService
from use_cases.exceptions import InvalidDocumentException
from use_cases.i_search_document_use_case import ISearchDocumentUseCase


class SearchDocumentUseCase(ISearchDocumentUseCase):

    @inject
    def __init__(self, nlp_method_service: INlpMethodService):
        self.nlp_method_service = nlp_method_service

    def get_documents_data(self, search_query: str, max_results: int, statistics: bool):
        try:
            documents_data = self.nlp_method_service.run_query(search_query, max_results, statistics)
            return documents_data
        except DiscoveryException:
            raise InvalidDocumentException()
