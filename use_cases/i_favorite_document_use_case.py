from abc import ABC, abstractmethod


class IFavoriteDocumentUseCase(ABC):

    @abstractmethod
    def store_document(self, data: dict): pass

    @abstractmethod
    def destroy_document(self, id: int): pass

    @abstractmethod
    def get_list_documents(self): pass
