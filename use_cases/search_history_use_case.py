from injector import inject
from datetime import datetime

from repositories.i_search_history_repository import ISeachHistoryRepository
from repositories.i_user_repository import IUserRepository
from entities.search_history import SearchHistory
from use_cases.i_search_history_use_case import ISearchHistoryUseCase
from use_cases.exceptions import InvalidSearchHistoryException, InvalidUserException


class SearchHistoryUseCase(ISearchHistoryUseCase):

    @inject
    def __init__(self, search_history_repository: ISeachHistoryRepository, user_repository: IUserRepository):
        self.search_history_repository = search_history_repository
        self.user_repository = user_repository

    def store_search_history(self, search: str, email: str):
        try:
            user = self.user_repository.get_user_by_email(email)
            search_history = SearchHistory(search,
                                           datetime.now(),
                                           user.id)
            self.search_history_repository.add(search_history)
            return True
        except InvalidUserException:
            pass
        except InvalidSearchHistoryException:
            raise InvalidSearchHistoryException

    def get_search_history(self):
        search_history = self.search_history_repository.get_all()
        return search_history
