from injector import inject
from itsdangerous import URLSafeTimedSerializer, BadTimeSignature, SignatureExpired

from environment_config import EnvironmentConfig
from repositories.i_user_repository import IUserRepository
from use_cases.exceptions import InvalidUserException
from use_cases.i_password_edit_use_case import IPasswordEditUseCase


class PasswordEditUseCase(IPasswordEditUseCase):

    @inject
    def __init__(self, user_repository: IUserRepository):
        self.user_repository = user_repository

    def update_password(self, email: str, password: str):
        user = self.user_repository.get_user_by_email(email)
        if user:
            user.change_password(password)
            self.user_repository.add(user)
            return True
        raise InvalidUserException()

    def verify_token(self, token: str, expiration: int = 600):
        password_reset_serializer = URLSafeTimedSerializer(EnvironmentConfig.SECRET_KEY)
        try:
            email = password_reset_serializer.loads(token,
                                                    salt='password-reset-salt',
                                                    max_age=expiration)
        except SignatureExpired:
            return None
        except BadTimeSignature:
            return None
        return email