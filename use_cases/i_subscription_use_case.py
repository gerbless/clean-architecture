from abc import ABC, abstractmethod


class ISubscriptionUseCase(ABC):

    @abstractmethod
    def get_subscription_data(self, email: str): pass

    @abstractmethod
    def add_customer(self, email: str): pass

    @abstractmethod
    def register_customer(self, email: str): pass

    @abstractmethod
    def get_card_register_status(self, token: str): pass

    @abstractmethod
    def add_subscription(self, email: str, plan_id: int, sector_id: int): pass
