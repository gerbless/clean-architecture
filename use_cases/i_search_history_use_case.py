from abc import ABC, abstractmethod


class ISearchHistoryUseCase(ABC):

    @abstractmethod
    def store_search_history(self, search: str, email: str): pass

    @abstractmethod
    def get_search_history(self): pass
