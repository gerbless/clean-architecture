from injector import inject

from entities.favorite_document import FavoriteDocument
from repositories.I_favorite_document_repository import IFavoriteDocumentRepository
from use_cases.i_favorite_document_use_case import IFavoriteDocumentUseCase
from use_cases.exceptions import InvalidFavoriteDocumentException


class FavoriteDocumentUseCase(IFavoriteDocumentUseCase):

    @inject
    def __init__(self, favorite_document_repository: IFavoriteDocumentRepository):
        self.favorite_document_repository = favorite_document_repository

    def store_document(self, data: dict):
        favorite_document = FavoriteDocument(data['title'],
                                             data['author'],
                                             data['description'],
                                             data['publication_date'],
                                             data['url'],
                                             data['host'],
                                             data['score'],
                                             data['user'])

        try:
            self.favorite_document_repository.add(favorite_document)
        except Exception:
            raise InvalidFavoriteDocumentException

    def destroy_document(self, uid: int):
        try:
            self.favorite_document_repository.delete(uid=uid)
        except Exception:
            raise InvalidFavoriteDocumentException

    def get_list_documents(self):
        list_documents = self.favorite_document_repository.get_all()
        return list_documents
