from abc import ABC, abstractmethod


class IPasswordEditUseCase(ABC):

    @abstractmethod
    def update_password(self, email: str, password: str): pass

    @abstractmethod
    def verify_token(self, token: str, expiration: int): pass