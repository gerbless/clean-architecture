from flask import url_for
from injector import inject
from itsdangerous import URLSafeTimedSerializer

from environment_config import EnvironmentConfig
from entities.user import User
from repositories.i_user_repository import IUserRepository
from use_cases.exceptions import InvalidUserException
from use_cases.i_register_use_case import IRegisterUseCase
from services.i_email_method_service import IEmailMethodService
from services.i_recaptcha_method_service import IRecaptchaMethodService
from services.exceptions import RecaptchaException


class RegisterUserCase(IRegisterUseCase):

    @inject
    def __init__(self, user_repository: IUserRepository, email_method_service: IEmailMethodService,
                 captcha_method_service: IRecaptchaMethodService):
        self.user_repository = user_repository
        self.email_method_service = email_method_service
        self.captcha_method_service = captcha_method_service

    def register_user(self, name: str, last_name: str, email: str, password: str, confirm_email: bool):
        try:
            self.user_repository.get_user_by_email(email)
        except InvalidUserException:
            user = User(name, last_name, email, password, confirm_email)
            self.user_repository.add(user)
            return user
        raise InvalidUserException

    def check_captcha(self, captcha: str):
        try:
            return self.captcha_method_service.check_human(captcha)
        except RecaptchaException:
            return False

    def get_captcha_key(self):
        return self.captcha_method_service.get_site_key()

    def generate_token(self, email: str):
        email_serializer = URLSafeTimedSerializer(EnvironmentConfig.SECRET_KEY)
        return email_serializer.dumps(email, salt='email-confirm-key')

    def generate_verify_url(self, email: str):
        return url_for('verify_account', token=self.generate_token(email), _external=True)

    def send_message(self, user: User, content: str):
        data_message = {}
        data_message['from'] = 'Mailgun <smartbird@sandbox704de7888292474390d02fc278a4fa71.mailgun.org>'
        data_message['to'] = user.email
        data_message['subject'] = 'Verificación de cuenta'
        data_message['content'] = content
        status = self.email_method_service.send_message(data_message)
        return status
