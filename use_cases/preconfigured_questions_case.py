from injector import inject

from entities.question import Question
from repositories.i_preconfigured_questions_repository import IPreconfiguredQuestionsRepository
from use_cases.i_preconfigured_questions_case import IPreconfiguredQuestionsUseCase
from use_cases.exceptions import InvalidQuestionsException


class PreconfiguredQuestionsUseCase(IPreconfiguredQuestionsUseCase):

    @inject
    def __init__(self, preconfigured_questions_repository: IPreconfiguredQuestionsRepository):
        self.preconfigured_questions_repository = preconfigured_questions_repository

    def store_question(self, data: dict):

            question = Question(data['question'],
                                data['business_sector'],
                                data['description'])
            self.preconfigured_questions_repository.add(question=question)

    def index_questions(self, uid: int = None):
        if uid is None:
            question = self.preconfigured_questions_repository.get_all()
        else:
            question = self.preconfigured_questions_repository.get_id(uid=uid)
        return question

    def destroy_question(self, uid: int):
        question = self.preconfigured_questions_repository.delete(uid=uid)
        return question

    def update_questions(self, data: dict):
        question = Question.obj_to_update(Question(data['question'], data['business_sector'], data['description']))
        return self.preconfigured_questions_repository.to_update(uid=data['id'], question=question)

    def search_question(self, question: str):
        questions = self.preconfigured_questions_repository.get_name_like(question=question)
        return questions
