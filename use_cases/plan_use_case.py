from injector import inject

from repositories.i_plan_repository import IPlanRepository
from use_cases.i_plan_use_case import IPlanUseCase


class PlanUseCase(IPlanUseCase):

    @inject
    def __init__(self, plan_repository: IPlanRepository):
        self.plan_repository = plan_repository

    def get_plans(self):
        return self.plan_repository.get_all()
