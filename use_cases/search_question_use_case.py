from injector import inject

from repositories.i_preconfigured_questions_repository import IPreconfiguredQuestionsRepository
from use_cases.i_search_question_use_case import ISearchQuestionUseCase


class SearchQuestionUseCase(ISearchQuestionUseCase):

    @inject
    def __init__(self, preconfigured_questions_repository: IPreconfiguredQuestionsRepository):
        self.preconfigured_questions_repository = preconfigured_questions_repository

    def get_search_question(self, search: str):
        questions = self.preconfigured_questions_repository.get_name_like(question=search)

        return questions
