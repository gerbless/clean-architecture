from abc import ABC, abstractmethod


class IVerifyAccountUseCase(ABC):

    @abstractmethod
    def verify_token(self, token: str, expiration: int): pass

    @abstractmethod
    def confirmation_email(self, email: str): pass

    @abstractmethod
    def is_confirmed_email(self, email: str): pass