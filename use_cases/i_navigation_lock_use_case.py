from abc import ABC, abstractmethod


class INavigationLockUseCase(ABC):

    @abstractmethod
    def verify_credit_card(self, email: str): pass

    @abstractmethod
    def verify_subscription(self, email: str): pass
