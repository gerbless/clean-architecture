from abc import ABC, abstractmethod


class IPreconfiguredQuestionsUseCase(ABC):

    @abstractmethod
    def store_question(self, data: dict):
        pass

    def index_questions(self, uid: int = None):
        pass

    def update_questions(self, data: dict):
        pass

    def destroy_question(self, uid: int):
        pass