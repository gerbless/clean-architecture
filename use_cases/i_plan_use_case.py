from abc import ABC, abstractmethod


class IPlanUseCase(ABC):

    @abstractmethod
    def get_plans(self): pass
