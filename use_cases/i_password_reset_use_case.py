from abc import ABC, abstractmethod


class IPasswordResetUseCase(ABC):

    @abstractmethod
    def send_message(self, email: str, content: str): pass

    @abstractmethod
    def generate_token(self, email: str): pass

    @abstractmethod
    def generate_password_url(self, email: str): pass
