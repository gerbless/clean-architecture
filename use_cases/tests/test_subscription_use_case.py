from unittest.mock import MagicMock

import pytest

from repositories.i_user_repository import IUserRepository
from repositories.i_subscription_repository import ISubscriptionRepository
from services.i_pay_method_service import IPayMethodService
from use_cases.exceptions import InvalidUserException
from use_cases.subscription_use_case import SubscriptionUseCase


def test_subscription_use_case_should_raise_error_if_user_does_not_exist():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    subscription_repository: ISubscriptionRepository = MagicMock(ISubscriptionRepository)
    user_repository.get_user_by_email.side_effect = InvalidUserException()
    subscription_use_case = SubscriptionUseCase(user_repository, subscription_repository, MagicMock(), MagicMock())
    with pytest.raises(InvalidUserException):
        subscription_use_case.get_subscription_data("correo@email.com")


def test_subscription_use_case_should_raise_error_if_costumer_does_not_exist():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    subscription_repository: ISubscriptionRepository = MagicMock(ISubscriptionRepository)
    pay_method_service: IPayMethodService = MagicMock(IPayMethodService)
    pay_method_service.get_user_payment_data.side_effect = InvalidUserException()
    subscription_use_case = SubscriptionUseCase(user_repository, subscription_repository, MagicMock(),
                                                pay_method_service)
    with pytest.raises(InvalidUserException):
        subscription_use_case.get_subscription_data("correo@email.com")


def test_subscription_use_case_should_return_dictionary_if_user_exist():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    pay_method_service: IPayMethodService = MagicMock(IPayMethodService)
    pay_method_service.get_user_payment_data.return_value = {
        "customerId": "cus_onoolldvec",
        "created": "2017-07-21 12:33:15",
        "email": "customer@gmail.com",
        "name": "Pedro Raul Perez",
        "creditCardType": "Visa",
        "last4CardDigits": "4425",
        "externalId": "14233531-8",
        "status": "1",
        "registerDate": "2017-07-21 14:22:01"
    }

    pay_method_service.register_customer.return_value = "https://www.flow.cl/app/webpay/disclaimer.php?token=41097C28B5BD78C77F589FE4BC59E18AC333F9EU"

    subscription_use_case = SubscriptionUseCase(user_repository, MagicMock(), MagicMock(), pay_method_service)
    subscription_dictionary = subscription_use_case.get_subscription_data("correo@email.com")
    assert subscription_dictionary == {
        "credit_cards": [
            {
                "creditCardType": "Visa",
                "last4CardDigits": "4425"
            }
        ],
        "new_credit_card_url": "https://www.flow.cl/app/webpay/disclaimer.php?token=41097C28B5BD78C77F589FE4BC59E18AC333F9EU"
    }


def test_subscription_use_case_add_customer_return_true_if_create_customer_is_success():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    pay_method_service: IPayMethodService = MagicMock(IPayMethodService)

    pay_method_service.create_customer.return_value = "cus_onoolldvec"

    subscription_use_case = SubscriptionUseCase(user_repository, MagicMock(), MagicMock(), pay_method_service)
    result = subscription_use_case.add_customer("correo@email.com")

    assert result is True


def test_subscription_use_case_add_customer_return_raise_error_if_user_does_not_exist():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    pay_method_service: IPayMethodService = MagicMock(IPayMethodService)

    user_repository.get_user_by_email.side_effect = InvalidUserException()
    subscription_use_case = SubscriptionUseCase(user_repository, MagicMock(), MagicMock(), pay_method_service)
    with pytest.raises(InvalidUserException):
        subscription_use_case.add_customer("correo@email.com")


def test_subscription_use_case_get_card_register_status_true_if_token_valid():
    pay_method_service: IPayMethodService = MagicMock(IPayMethodService)
    pay_method_service.get_register_status_customer.return_value = {
        "status": "1",
        "customerId": "cus_onoolldvec",
        "creditCardType": "Visa",
        "last4CardDigits": "0366"
    }

    subscription_use_case = SubscriptionUseCase(MagicMock(), MagicMock(), MagicMock(), pay_method_service)
    result = subscription_use_case.get_card_register_status("41097C28B5BD78C77F589FE4BC59E18AC333F9EU")
    assert result == True
