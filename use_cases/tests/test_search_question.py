from unittest.mock import MagicMock
import pytest

from app import create_app
from repositories.i_preconfigured_questions_repository import IPreconfiguredQuestionsRepository
from use_cases.search_question_use_case import SearchQuestionUseCase
from use_cases.exceptions import InvalidSearchQuestionException
from injector import Binder

question_repository: IPreconfiguredQuestionsRepository = MagicMock(IPreconfiguredQuestionsRepository)


def configure_bindings(binder: Binder) -> Binder:
    binder.bind(IPreconfiguredQuestionsRepository, to=question_repository)

@pytest.fixture(scope="session")
def init():
    app = create_app(modules=[configure_bindings])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client


def test_search_question_use_case_return_find_question_format_successful(init):
    question: dict = [{
        "id": 1,
        "question": "My question",
        "description": "My description",
        "business_sector_id": 1,
        "business_sector": {
            "id": 1,
            "name_sector": "legal",
            "type": None
        }
    }, {
        "id": 1,
        "question": "My question",
        "description": "My description",
        "business_sector_id": 1,
        "business_sector": {
            "id": 1,
            "name_sector": "legal",
            "type": None
        }
    }]
    question_repository.get_name_like.return_value = question
    search_question_use_case = SearchQuestionUseCase(question_repository)
    result_search_questions = search_question_use_case.get_search_question(search='My Quies')
    assert result_search_questions == question


def test_search_question_use_case_should_raise_error_if_question_do_not_exist(init):
    question_repository.get_name_like.side_effect = InvalidSearchQuestionException(message_user='Ocurrio un Error',
                                                                                       err_debug='err_debug')
    search_question_use_case = SearchQuestionUseCase(question_repository)
    with pytest.raises(InvalidSearchQuestionException):
        search_question_use_case.get_search_question(search='My Quies')
