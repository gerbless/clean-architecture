from unittest.mock import MagicMock

from entities.user import User
from repositories.i_user_repository import IUserRepository
from use_cases.exceptions import InvalidPasswordException
from use_cases.login_use_case import LoginUseCase


def test_login_use_case_return_false_if_password_do_not_valid():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    user: User = MagicMock(User)
    user.valid_credential.side_effect = InvalidPasswordException()
    user_repository.get_user_by_email.return_value = user
    login_use_case = LoginUseCase(user_repository)

    valid_login = login_use_case.valid_credential("email@email.com", "password")
    assert valid_login is False


def test_login_use_case_should_return_true():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    user: User = MagicMock(User)
    user.valid_credential.return_value = True
    user_repository.get_user_by_email.return_value = user
    login_use_case = LoginUseCase(user_repository)

    valid_login = login_use_case.valid_credential("email@email.com", "password")
    assert valid_login is True

