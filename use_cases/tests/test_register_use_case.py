from unittest.mock import MagicMock

import pytest

from entities.user import User
from repositories.i_user_repository import IUserRepository
from use_cases.register_use_case import RegisterUserCase
from use_cases.exceptions import InvalidUserException
from services.exceptions import RecaptchaException
from services.i_email_method_service import IEmailMethodService
from services.i_recaptcha_method_service import IRecaptchaMethodService


def test_register_use_case_return_false_if_email_or_user_exist():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    user_repository.get_user_by_email.return_value = 1
    register_use_case = RegisterUserCase(user_repository, MagicMock(), MagicMock())
    with pytest.raises(InvalidUserException):
        register_use_case.register_user(name='usuario', last_name='apellido', email='correo@gmail.com', password='123',
                                        confirm_email=False)


def test_register_use_case_successfully_created_user():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    user_repository.add = MagicMock()
    user_repository.get_user_by_email.side_effect = InvalidUserException()
    register_use_case = RegisterUserCase(user_repository, MagicMock(), MagicMock())
    register_use_case.register_user(name='usuario', last_name='apellido', email='correo@gmail.com', password='123',
                                    confirm_email=False)
    user_repository.add.assert_called_once()


def test_register_use_case_check_captcha_should_return_false_if_invalid_check_human():
    captcha_method_service: IRecaptchaMethodService = MagicMock(IRecaptchaMethodService)
    captcha_method_service.check_human.side_effect = RecaptchaException()
    register_use_case = RegisterUserCase(MagicMock(), MagicMock(), captcha_method_service)
    assert register_use_case.check_captcha("captcha-demo") is False


def test_register_use_case_service_mail_err_400():
    email_method_service: IEmailMethodService = MagicMock(IEmailMethodService)
    email_method_service.send_message.return_value = b'parameters are missing'
    user = User("juan", "Perez", "email@email.com", "password", False)
    register_use_case = RegisterUserCase(MagicMock(), email_method_service, MagicMock())
    result = register_use_case.send_message(user, 'Verificación de cuenta')
    assert result == b'parameters are missing'


def test_register_use_case_service_mail_200():
    email_method_service: IEmailMethodService = MagicMock(IEmailMethodService)
    email_method_service.send_message.return_value = True
    user = User("juan", "Perez", "email@email.com", "password", False)
    register_use_case = RegisterUserCase(MagicMock(), email_method_service, MagicMock())
    result = register_use_case.send_message(user, 'Verificación de cuenta')
    assert result == True
