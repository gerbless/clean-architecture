from unittest.mock import MagicMock
import pytest
import time

from entities.user import User
from repositories.i_user_repository import IUserRepository
from use_cases.exceptions import InvalidUserException
from use_cases.verify_account_use_case import VerifyAccountUseCase
from use_cases.register_use_case import RegisterUserCase


def test_verify_account_use_case_confirmation_email_should_raise_error_if_email_do_not_exist():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    user_repository.get_user_by_email.return_value = None
    verify_account_use_case = VerifyAccountUseCase(user_repository)
    with pytest.raises(InvalidUserException):
        verify_account_use_case.confirmation_email("email@email.com")


def test_verify_account_use_case_confirmation_email_should_update_user():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    initial_user = User("juan", "Perez", "email@email.com", "password", False)
    user_repository.get_user_by_email.return_value = initial_user
    verify_account_use_case = VerifyAccountUseCase(user_repository)
    verify_account_use_case.confirmation_email("email@email.com")
    user_repository.get_user_by_email.assert_called_once_with("email@email.com")
    user_repository.add.assert_called_once()


def test_verify_use_case_is_confirmed_email_should_raise_error_email_invalid():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    user_repository.get_user_by_email.side_effect = InvalidUserException()
    verify_account_use_case = VerifyAccountUseCase(user_repository)
    with pytest.raises(InvalidUserException):
        verify_account_use_case.is_confirmed_email("email@email.com")


def test_verify_account_use_case_verify_token():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    register_use_case = RegisterUserCase(user_repository, MagicMock(), MagicMock())
    verify_account_use_case = VerifyAccountUseCase(user_repository)
    token = register_use_case.generate_token("email@email.com")
    email = verify_account_use_case.verify_token(token)
    assert email == "email@email.com"


def test_verify_account_use_case_invalid_token():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    register_use_case = RegisterUserCase(user_repository, MagicMock(), MagicMock())
    verify_account_use_case = VerifyAccountUseCase(user_repository)
    token = register_use_case.generate_token("email@email.com")
    email = verify_account_use_case.verify_token(token)
    assert email != "admin@email.com"


def test_verify_account_use_case_confirm_token_expired_token():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    register_use_case = RegisterUserCase(user_repository, MagicMock(), MagicMock())
    verify_account_use_case = VerifyAccountUseCase(user_repository)
    token = register_use_case.generate_token("email@email.com")
    time.sleep(1)
    email = verify_account_use_case.verify_token(token, 0)
    assert email == None
