from unittest.mock import MagicMock

import pytest

from use_cases.preconfigured_questions_case import PreconfiguredQuestionsUseCase
from repositories.i_preconfigured_questions_repository import IPreconfiguredQuestionsRepository
from use_cases.exceptions import InvalidQuestionsException


def test_preconfigured_questions_get_return_false_or_not_exist():
    preconfigured_questions_repository: IPreconfiguredQuestionsRepository = MagicMock(IPreconfiguredQuestionsRepository)
    preconfigured_questions_repository.get_all.side_effect = InvalidQuestionsException(message_user='Ocurrio un Error',
                                                                                       err_debug='err_debug')
    preconfigured_questions_use_case = PreconfiguredQuestionsUseCase(preconfigured_questions_repository)
    with pytest.raises(InvalidQuestionsException):
        preconfigured_questions_use_case.index_questions()



def test_preconfigured_questions_save_return_false():
    preconfigured_questions_repository: IPreconfiguredQuestionsRepository = MagicMock(IPreconfiguredQuestionsRepository)
    preconfigured_questions_repository.add.side_effect = InvalidQuestionsException(message_user='Ocurrio un Error',
                                                                                       err_debug='err_debug')
    preconfigured_questions_use_case = PreconfiguredQuestionsUseCase(preconfigured_questions_repository)
    with pytest.raises(InvalidQuestionsException):
        preconfigured_questions_use_case.store_question(data={'question': 'my question', 'business_sector': 1,
                                                              'description': 'sector legal'})



def test_preconfigured_questions_get_id_return_false():
    preconfigured_questions_repository: IPreconfiguredQuestionsRepository = MagicMock(IPreconfiguredQuestionsRepository)
    preconfigured_questions_repository.get_id.side_effect = InvalidQuestionsException(message_user='Ocurrio un Error',
                                                                                       err_debug='err_debug')
    preconfigured_questions_use_case = PreconfiguredQuestionsUseCase(preconfigured_questions_repository)
    with pytest.raises(InvalidQuestionsException):
        preconfigured_questions_use_case.index_questions(uid=1)


def test_preconfigured_questions_delete_return_false():
    preconfigured_questions_repository: IPreconfiguredQuestionsRepository = MagicMock(IPreconfiguredQuestionsRepository)
    preconfigured_questions_repository.delete.side_effect = InvalidQuestionsException(message_user='Ocurrio un Error',
                                                                                       err_debug='err_debug')
    preconfigured_questions_use_case = PreconfiguredQuestionsUseCase(preconfigured_questions_repository)
    with pytest.raises(InvalidQuestionsException):
        preconfigured_questions_use_case.destroy_question(uid=1)


def test_preconfigured_questions_update_return_false():
    preconfigured_questions_repository: IPreconfiguredQuestionsRepository = MagicMock(IPreconfiguredQuestionsRepository)
    preconfigured_questions_repository.to_update.side_effect = InvalidQuestionsException(message_user='Ocurrio un Error',
                                                                                       err_debug='err_debug')
    preconfigured_questions_use_case = PreconfiguredQuestionsUseCase(preconfigured_questions_repository)
    with pytest.raises(InvalidQuestionsException):
        preconfigured_questions_use_case.update_questions(data={'id': 1, 'question': 'my question',
                                                                'business_sector': 1, 'description': 'sector legal'})


def test_preconfigured_questions_find_question_successful():
    question: dict = [{
        "id": 1,
        "question": "My question",
        "description": "My description",
        "business_sector_id": 1,
        "business_sector": {
            "id": 1,
            "name_sector": "legal",
            "type": None
        }
    }]
    preconfigured_questions_repository: IPreconfiguredQuestionsRepository = MagicMock(IPreconfiguredQuestionsRepository)
    preconfigured_questions_repository.get_name_like.return_value = question

    preconfigured_questions_use_case = PreconfiguredQuestionsUseCase(preconfigured_questions_repository)
    result_search_question = preconfigured_questions_use_case.search_question(question='My question')
    assert result_search_question == question


def test_preconfigured_questions_find_question__return_false():
    preconfigured_questions_repository: IPreconfiguredQuestionsRepository = MagicMock(IPreconfiguredQuestionsRepository)
    preconfigured_questions_repository.get_name_like.side_effect = InvalidQuestionsException(
        message_user='Ocurrio un Error', err_debug='err_debug')
    preconfigured_questions_use_case = PreconfiguredQuestionsUseCase(preconfigured_questions_repository)
    with pytest.raises(InvalidQuestionsException):
        preconfigured_questions_use_case.search_question(question='My question')
