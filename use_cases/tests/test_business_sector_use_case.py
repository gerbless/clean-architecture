from unittest.mock import MagicMock

import pytest

from repositories.i_business_sector_repository import IBusinessSectorRepository
from repositories.i_user_repository import IUserRepository
from use_cases.business_sector_use_case import BusinessSectorUseCase
from use_cases.exceptions import InvalidBusinessSectorException


def test_business_sector_use_case_get_business_sectors_should_return_zero_if_get_business_sectors_is_empty():
    business_sector_repository: IBusinessSectorRepository = MagicMock(IBusinessSectorRepository)
    user_repository: IUserRepository = MagicMock(IUserRepository)
    business_sector_use_case = BusinessSectorUseCase(business_sector_repository, user_repository)
    assert len(business_sector_use_case.get_business_sectors()) is 0


def test_business_sector_method_index_business_sector_get_return_false_or_not_exist():
    business_sector_repository: IBusinessSectorRepository = MagicMock(IBusinessSectorRepository)
    user_repository: IUserRepository = MagicMock(IUserRepository)
    business_sector_repository.get_all.side_effect = InvalidBusinessSectorException()
    business_sector_use_case = BusinessSectorUseCase(business_sector_repository, user_repository)
    with pytest.raises(InvalidBusinessSectorException):
        business_sector_use_case.index_business_sector()
