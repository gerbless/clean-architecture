from unittest.mock import MagicMock

import pytest

from services.exceptions import DiscoveryException
from services.i_nlp_method_service import INlpMethodService
from use_cases.exceptions import InvalidDocumentException
from use_cases.search_document_use_case import SearchDocumentUseCase


def test_search_document_use_case_should_raise_error_if_discovery_failure():
    nlp_method_service: INlpMethodService = MagicMock(INlpMethodService)
    nlp_method_service.run_query.side_effect = DiscoveryException()
    search_document_use_case = SearchDocumentUseCase(nlp_method_service)
    with pytest.raises(InvalidDocumentException):
        search_document_use_case.get_documents_data("top music english", 20, False)


def test_search_document_use_case_return_dictionary_of_documents_if_query_is_send_successfully():
    nlp_method_service: INlpMethodService = MagicMock(INlpMethodService)
    nlp_method_service.run_query.return_value = {
        "matching_results": 2315,
        "passages": [],
        "results": [
            {
                "id": "GoXBk6orGKTN7h",
                "text": "El top 10 del jueves 4 quedó así (Fuente: Ultracine)",
                "publication_date": "2018-10-05T15:43:00Z",
                "title": "Lele Pons se prepara para lanzar otro sencillo este año",
                "result_metadata": {
                    "score": 20
                }
            }
        ]
    }

    search_document_use_case = SearchDocumentUseCase(nlp_method_service)
    documents_dictionary = search_document_use_case.get_documents_data("top music english", 20, False)
    assert documents_dictionary == {
        "matching_results": 2315,
        "passages": [],
        "results": [
            {
                "id": "GoXBk6orGKTN7h",
                "text": "El top 10 del jueves 4 quedó así (Fuente: Ultracine)",
                "publication_date": "2018-10-05T15:43:00Z",
                "title": "Lele Pons se prepara para lanzar otro sencillo este año",
                "result_metadata": {
                    "score": 20
                }
            }
        ]
    }


def test_search_document_use_case_return_dictionary_of_documents_and_statistics_if_query_is_send_successfully():
    nlp_method_service: INlpMethodService = MagicMock(INlpMethodService)
    nlp_method_service.run_query.return_value = {
        "matching_results": 2315,
        "passages": [],
        "aggregations": [
            {
                "type": "term",
                "field": "enriched_text.sentiment.document.label",
                "results": [
                    {
                        "key": "positive",
                        "matching_results": 42996
                    },
                    {
                        "key": "negative",
                        "matching_results": 8420
                    },
                    {
                        "key": "neutral",
                        "matching_results": 787
                    }
                ]
            }
        ],
        "results": [
            {
                "id": "GoXBk6orGKTN7h",
                "text": "El top 10 del jueves 4 quedó así (Fuente: Ultracine)",
                "publication_date": "2018-10-05T15:43:00Z",
                "title": "Lele Pons se prepara para lanzar otro sencillo este año",
                "result_metadata": {
                    "score": 20
                }
            }
        ]
    }

    search_document_use_case = SearchDocumentUseCase(nlp_method_service)
    documents_dictionary = search_document_use_case.get_documents_data("top music english", 20, True)
    assert documents_dictionary == {
        "matching_results": 2315,
        "passages": [],
        "aggregations": [
            {
                "type": "term",
                "field": "enriched_text.sentiment.document.label",
                "results": [
                    {
                        "key": "positive",
                        "matching_results": 42996
                    },
                    {
                        "key": "negative",
                        "matching_results": 8420
                    },
                    {
                        "key": "neutral",
                        "matching_results": 787
                    }
                ]
            }
        ],
        "results": [
            {
                "id": "GoXBk6orGKTN7h",
                "text": "El top 10 del jueves 4 quedó así (Fuente: Ultracine)",
                "publication_date": "2018-10-05T15:43:00Z",
                "title": "Lele Pons se prepara para lanzar otro sencillo este año",
                "result_metadata": {
                    "score": 20
                }
            }
        ]
    }
