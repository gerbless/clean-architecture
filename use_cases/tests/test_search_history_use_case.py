from unittest.mock import MagicMock

import pytest

from repositories.i_search_history_repository import ISeachHistoryRepository
from repositories.i_user_repository import IUserRepository
from use_cases.exceptions import InvalidSearchHistoryException
from use_cases.search_history_use_case import SearchHistoryUseCase


def test_search_history_use_case_store_search_history_should_raise_error_if_invalid_search():
    search_history_repository: ISeachHistoryRepository = MagicMock(ISeachHistoryRepository)
    search_history_repository.add.side_effect = InvalidSearchHistoryException()
    search_history_use_case = SearchHistoryUseCase(search_history_repository, MagicMock())
    with pytest.raises(InvalidSearchHistoryException):
        search_history_use_case.store_search_history("Top musical march", "admin@gmail.com")


def test_search_history_use_case_store_search_history_should_success_if_valid_search():
    search_history_repository: ISeachHistoryRepository = MagicMock(ISeachHistoryRepository)
    user_repository: IUserRepository = MagicMock(IUserRepository)
    search_history_use_case = SearchHistoryUseCase(search_history_repository, user_repository)
    search_history_use_case.store_search_history("Top musical march", "admin@gmail.com")


def test_search_history_use_case_get_search_history_should_zero_if_search_history_is_empty():
    search_history_repository: ISeachHistoryRepository = MagicMock(ISeachHistoryRepository)
    user_repository: IUserRepository = MagicMock(IUserRepository)
    search_history_use_case = SearchHistoryUseCase(search_history_repository, user_repository)
    assert len(search_history_use_case.get_search_history()) is 0
