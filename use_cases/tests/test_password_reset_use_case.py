from unittest.mock import MagicMock

import pytest

from app import create_app
from entities.user import User
from repositories.i_user_repository import IUserRepository
from services.i_email_method_service import IEmailMethodService
from use_cases.exceptions import InvalidUserException
from use_cases.password_reset_use_case import PasswordResetUseCase


def test_password_reset_use_case_send_message_should_raise_invalid_user_exception():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    user_repository.get_user_by_email.return_value = None
    password_reset_use_case = PasswordResetUseCase(user_repository, MagicMock())
    with pytest.raises(InvalidUserException):
        password_reset_use_case.send_message("email@email.com", "Reset password")


def test_password_reset_use_case_send_message_should_send_and_email():
    app = create_app(modules=[])
    user_repository: IUserRepository = MagicMock(IUserRepository)
    email_service: IEmailMethodService = MagicMock(IEmailMethodService)
    user_repository.get_user_by_email.return_value = User("first_name", "last_name", "email@email.com", "password", True)
    password_reset_use_case = PasswordResetUseCase(user_repository, email_service)
    with app.test_request_context():
        password_reset_use_case.send_message("email@email.com", "Reset password")
    email_service.send_message.assert_called_once()


def test_password_reset_use_case_token_is_unique():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    email_service: IEmailMethodService = MagicMock(IEmailMethodService)
    password_reset_use_case = PasswordResetUseCase(user_repository, email_service)
    token_1 = password_reset_use_case.generate_token("email@email.com")
    token_2 = password_reset_use_case.generate_token("admin@email.com")
    assert token_1 != token_2