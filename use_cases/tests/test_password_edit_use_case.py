from unittest.mock import MagicMock
import pytest
import time

from entities.user import User
from repositories.i_user_repository import IUserRepository
from services.i_email_method_service import IEmailMethodService
from use_cases.exceptions import InvalidUserException
from use_cases.password_edit_use_case import PasswordEditUseCase
from use_cases.password_reset_use_case import PasswordResetUseCase


def test_password_edit_use_case_update_password_should_raise_error_if_email_do_not_exist():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    user_repository.get_user_by_email.return_value = None
    password_edit_use_case = PasswordEditUseCase(user_repository)
    with pytest.raises(InvalidUserException):
        password_edit_use_case.update_password("email@email.com", "password")


def test_password_edit_use_case_update_password_should_update_user():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    initial_user = User("juan", "Perez", "email@email.com", "password", True)
    user_repository.get_user_by_email.return_value = initial_user
    password_reset_use_case = PasswordEditUseCase(user_repository)
    password_reset_use_case.update_password("email@email.com", "password2")
    user_repository.get_user_by_email.assert_called_once_with("email@email.com")
    user_repository.add.assert_called_once()


def test_password_edit_use_case_verify_token():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    email_service: IEmailMethodService = MagicMock(IEmailMethodService)
    password_reset_use_case = PasswordResetUseCase(user_repository, email_service)
    password_edit_use_case = PasswordEditUseCase(user_repository)
    token = password_reset_use_case.generate_token("email@email.com")
    email = password_edit_use_case.verify_token(token)
    assert email == "email@email.com"


def test_password_edit_use_case_invalid_token():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    email_service: IEmailMethodService = MagicMock(IEmailMethodService)
    password_reset_use_case = PasswordResetUseCase(user_repository, email_service)
    password_edit_use_case = PasswordEditUseCase(user_repository)
    token = password_reset_use_case.generate_token("email@email.com")
    email = password_edit_use_case.verify_token(token)
    assert email != "admin@email.com"


def test_password_edit_use_case_confirm_token_expired_token():
    user_repository: IUserRepository = MagicMock(IUserRepository)
    email_service: IEmailMethodService = MagicMock(IEmailMethodService)
    password_reset_use_case = PasswordResetUseCase(user_repository, email_service)
    password_edit_use_case = PasswordEditUseCase(user_repository)
    token = password_reset_use_case.generate_token("email@email.com")
    time.sleep(1)
    email = password_edit_use_case.verify_token(token, 0)
    assert email == None
