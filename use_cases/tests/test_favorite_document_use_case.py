from unittest.mock import MagicMock

import pytest

from use_cases.favorite_document_use_case import FavoriteDocumentUseCase
from repositories.I_favorite_document_repository import IFavoriteDocumentRepository
from use_cases.exceptions import InvalidFavoriteDocumentException


def test_favorite_document_use_case_store_document_should_raise_error_if_invalid_document():
    favorite_document_repository: IFavoriteDocumentRepository = MagicMock(IFavoriteDocumentRepository)
    favorite_document_repository.add.side_effect = InvalidFavoriteDocumentException()
    favorite_document_use_case = FavoriteDocumentUseCase(favorite_document_repository)
    with pytest.raises(InvalidFavoriteDocumentException):
        data = {"title": "Top musical march", "author": "Martin Stein",
                "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                "publication_date": "2018-11-13T07:16:00Z", "url": "https://www.lipsum.com/xyz",
                "host": "https://www.lipsum.com", "score": 2.5, "user": 5}
        favorite_document_use_case.store_document(data)


def test_favorite_document_use_case_destroy_document_should_raise_error_if_invalid_document():
    favorite_document_repository: IFavoriteDocumentRepository = MagicMock(IFavoriteDocumentRepository)
    favorite_document_repository.delete.side_effect = InvalidFavoriteDocumentException()
    favorite_document_use_case = FavoriteDocumentUseCase(favorite_document_repository)
    with pytest.raises(InvalidFavoriteDocumentException):
        favorite_document_use_case.destroy_document(uid=2)


def test_favorite_document_use_case_get_list_documents_should_zero_if_favorite_documents_is_empty():
    favorite_document_repository: IFavoriteDocumentRepository = MagicMock(IFavoriteDocumentRepository)
    favorite_document_use_case = FavoriteDocumentUseCase(favorite_document_repository)
    assert len(favorite_document_use_case.get_list_documents()) is 0
