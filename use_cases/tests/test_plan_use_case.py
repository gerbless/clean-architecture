from unittest.mock import MagicMock

from repositories.i_plan_repository import IPlanRepository
from use_cases.plan_use_case import PlanUseCase


def test_plan_use_case_get_plans_should_return_zero_if_get_plans_is_empty():
    plan_repository: IPlanRepository = MagicMock(IPlanRepository)
    plan_use_case = PlanUseCase(plan_repository)
    assert len(plan_use_case.get_plans()) is 0
