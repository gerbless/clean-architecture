from abc import ABC, abstractmethod


class IResendVerifyAccountUseCase(ABC):

    @abstractmethod
    def send_message(self, email: str, content: str): pass

    @abstractmethod
    def generate_verify_url(self, email): pass
