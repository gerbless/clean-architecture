from abc import ABC, abstractmethod
from entities.plan import Plan


class IPlanRepository(ABC):

    @abstractmethod
    def add(self, plan: Plan): pass

    @abstractmethod
    def get_all(self): pass

    @abstractmethod
    def get_id(self, uid: int): pass
