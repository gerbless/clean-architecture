from abc import ABC, abstractmethod

from entities.business_sector import BusinessSector


class IBusinessSectorRepository(ABC):

    @abstractmethod
    def add(self, business_sector: BusinessSector):
        pass

    @abstractmethod
    def get_all(self):
        pass

    def get_id(self, uid: int):
        pass

    def get_qty_business_sectors_by_user(self, uid: int):
        pass

    def delete(self, uid: int):
        pass
