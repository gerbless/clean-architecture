from abc import ABC, abstractmethod

from entities.question import Question


class IPreconfiguredQuestionsRepository(ABC):

    @abstractmethod
    def add(self, question: Question): pass

    @abstractmethod
    def get_all(self): pass

    @abstractmethod
    def get_id(self, uid: int): pass

    @abstractmethod
    def delete(self, uid: int): pass

    @abstractmethod
    def to_update(self, uid: int, question: Question): pass

    @abstractmethod
    def get_name_like(self, question: str): pass
