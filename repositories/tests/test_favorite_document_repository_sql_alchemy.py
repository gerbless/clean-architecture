import pytest

from repositories.tests.pytest_fixture_db import db
from repositories.tests.queries.queries_users import create_user, get_user_all
from repositories.tests.queries.queries_favorite_document import *
from use_cases.exceptions import InvalidFavoriteDocumentException

db = db


def user(db):
    create_user(db=db)
    users = get_user_all(db=db)
    return users[0].id


def test_favorite_document_repository_sqlalchemy_add_should_persist_a_favorite_document(db):
    user_id = user(db=db)
    favorite_documents_recovered = create_favorite_document(db=db, user_id=user_id)
    assert len(favorite_documents_recovered) is 1


def test_favorite_document_repository_sqlalchemy_view_favorite_document_by_id_should_return_a_favorite_document(db):
    user_id = user(db=db)
    favorite_documents_recovered = create_favorite_document(db=db, user_id=user_id)
    favorite_documents_id = favorite_documents_recovered[0].id
    favorite_document_recover = get_favorite_document_by_id(db=db, uid=favorite_documents_id)
    assert type(favorite_document_recover) is FavoriteDocument
    assert favorite_document_recover.title == favorite_documents_recovered[0].title
    assert favorite_document_recover.author == favorite_documents_recovered[0].author
    assert favorite_document_recover.url == favorite_documents_recovered[0].url
    assert favorite_document_recover.description == favorite_documents_recovered[0].description
    assert favorite_document_recover.publication_date == favorite_documents_recovered[0].publication_date
    assert favorite_document_recover.host == favorite_documents_recovered[0].host
    assert favorite_document_recover.score == favorite_documents_recovered[0].score
    assert favorite_document_recover.user_id == favorite_documents_recovered[0].user_id
    assert favorite_document_recover.user.email == favorite_documents_recovered[0].user.email
    assert favorite_document_recover.user.id == favorite_documents_recovered[0].user.id
    assert favorite_document_recover.user.first_name == favorite_documents_recovered[0].user.first_name
    assert favorite_document_recover.user.last_name == favorite_documents_recovered[0].user.last_name


def test_favorite_document_repository_sqlalchemy_view_favorite_document_by_id_should_raise_error_if_favorite_document_does_not_exist(
        db):
    user_id = user(db=db)
    create_favorite_document(db=db, user_id=user_id)
    with pytest.raises(InvalidFavoriteDocumentException):
        favorite_document_recover = get_favorite_document_by_id(db=db, uid=2)
        return favorite_document_recover


def test_favorite_document_repository_sqlalchemy_delete_should_persist_a_favorite_document(db):
    user_id = user(db=db)
    favorite_documents_recovered = create_favorite_document(db=db, user_id=user_id)
    favorite_documents_id = favorite_documents_recovered[0].id
    delete_favorite_document(db=db, uid=favorite_documents_id)
    favorite_documents_recovered = get_favorite_document_all(db=db)
    assert len(favorite_documents_recovered) is 0


def test_favorite_document_repository_sqlalchemy_delete_favorite_document_by_id_should_raise_error_if_favorite_document_does_not_exist(
        db):
    user_id = user(db=db)
    create_favorite_document(db=db, user_id=user_id)
    with pytest.raises(InvalidFavoriteDocumentException):
        delete_favorite_document(db=db, uid=3)


def test_favorite_document_repository_sqlalchemy_get_all_should_persist_list_favorite_documents(db):
    user_id = user(db=db)
    for x in range(0, 3):
        create_favorite_document(db=db, user_id=user_id)

    favorite_documents_recovered = get_favorite_document_all(db=db)
    assert type(favorite_documents_recovered) is list
    assert len(favorite_documents_recovered) is 3
