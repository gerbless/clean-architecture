from repositories.tests.pytest_fixture_db import db
from repositories.tests.queries.queries_plans import create_plan, get_plan_all

db = db


def test_plan_repository_sqlalchemy_add_should_persist_a_plan(db):
    plan_recovered = create_plan(db=db)
    assert len(plan_recovered) is 1


def test_plan_repository_sqlalchemy_get_all_should_persist_list_plans(db):
    for i in range(1, 4):
        create_plan(db=db)

    plan_recovered = get_plan_all(db=db)
    assert type(plan_recovered) is list
    assert len(plan_recovered) is 3
