import requests
from entities.question import Question
from repositories.sql_alchemy.preconfigured_question_repository_sql_alchemy import \
    PreconfiguredQuestionRepositorySqlAlchemy

def get_preconfigured_question_all(db):
    preconfigured_question_repository = PreconfiguredQuestionRepositorySqlAlchemy(db)
    return preconfigured_question_repository.get_all()


def create_preconfigured_question(db, business_sector_id: int, multiples_questions: bool = False):
    preconfigured_question_repository = PreconfiguredQuestionRepositorySqlAlchemy(db)
    preconfigured_question_repository.add(Question('My question', business_sector_id, 'my description'))
    if multiples_questions:
        multiple_questions(preconfigured_question_repository=preconfigured_question_repository,
                           business_sector_id=business_sector_id)
    return get_preconfigured_question_all(db)


def delete_preconfigured_question(db, uid: int):
    preconfigured_question_repository = PreconfiguredQuestionRepositorySqlAlchemy(db)
    preconfigured_question = preconfigured_question_repository.delete(uid=uid)
    return preconfigured_question


def get_like_question(db, question: str):
    preconfigured_question_repository = PreconfiguredQuestionRepositorySqlAlchemy(db)
    like_question = preconfigured_question_repository.get_name_like(question=question)
    return like_question


# crea  25000 registros en questions
def multiple_questions(preconfigured_question_repository: PreconfiguredQuestionRepositorySqlAlchemy,
                       business_sector_id: int):
    word_site = "http://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain"
    response = requests.get(word_site)
    words = response.content.splitlines()
    for x in range(0, 10):
        preconfigured_question_repository.add(Question(words[x].decode("utf-8"), business_sector_id,
                                                       words[x].decode("utf-8")))
