from datetime import datetime
from entities.search_history import SearchHistory
from repositories.sql_alchemy.search_history_repository_sql_alchemy import SearchHistoryRepositorySqlAlchemy


def get_search_history_all(db):
    search_history_repository = SearchHistoryRepositorySqlAlchemy(db)
    return search_history_repository.get_all()


def create_search_history(db, user_id: int):
    search_history_repository = SearchHistoryRepositorySqlAlchemy(db)
    search_history_initial = SearchHistory("Top musical november", datetime.now(), user_id)
    search_history_repository.add(search_history_initial)
    return get_search_history_all(db)
