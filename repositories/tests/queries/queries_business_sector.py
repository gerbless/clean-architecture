from entities.business_sector import BusinessSector
from repositories.sql_alchemy.business_sector_repository_sql_alchemy import BusinessSectorRepositorySqlAlchemy


def get_business_sector_all(db):
    business_sector_repository = BusinessSectorRepositorySqlAlchemy(db)
    return business_sector_repository.get_all()


def get_business_sector_by_id(db, uid: int):
    business_sector_repository = BusinessSectorRepositorySqlAlchemy(db)
    return business_sector_repository.get_id(uid=uid)


def get_qty_business_sectors_by_user(db, uid: int):
    business_sector_repository = BusinessSectorRepositorySqlAlchemy(db)
    return business_sector_repository.get_qty_business_sectors_by_user(uid=uid)


def create_business_sector(db):
    business_sector_repository = BusinessSectorRepositorySqlAlchemy(db)
    business_sector_repository.add(BusinessSector("legal", "tipo", "Proin feugiat ipsum in diam finibus volutpat."))
    return get_business_sector_all(db)


def delete_business_sector(db, uid: int):
    business_sector_repository = BusinessSectorRepositorySqlAlchemy(db)
    return business_sector_repository.delete(uid=uid)
