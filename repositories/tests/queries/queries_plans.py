from entities.plan import Plan
from repositories.sql_alchemy.plan_repository_sql_alchemy import PlanRepositorySqlAlchemy


def get_plan_all(db):
    plan_repository = PlanRepositorySqlAlchemy(db)
    return plan_repository.get_all()


def create_plan(db):
    plan_repository = PlanRepositorySqlAlchemy(db)
    plan_initial = Plan("Plan junior", 20000.00, "CLP", 3, "Proin feugiat ipsum in diam finibus volutpat.")
    plan_repository.add(plan_initial)
    return get_plan_all(db)
