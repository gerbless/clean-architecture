from entities.subscription import Subscription
from repositories.sql_alchemy.subscription_repository_sql_alchemy import SubscriptionRepositorySqlAlchemy


def get_subscription_all(db):
    subscription_repository = SubscriptionRepositorySqlAlchemy(db)
    return subscription_repository.get_all()


def create_subscription(db, user_id: int, business_sector_id: int, plan_id: int):
    subscription_repository = SubscriptionRepositorySqlAlchemy(db)
    subscription_initial = Subscription(user_id, business_sector_id, plan_id, "sus_azcyjj9ycd")
    subscription_repository.add(subscription_initial)
    return get_subscription_all(db)
