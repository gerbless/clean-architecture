from repositories.tests.pytest_fixture_db import db
from repositories.tests.queries.queries_search_historial import create_search_history, get_search_history_all
from repositories.tests.queries.queries_users import create_user, get_user_all

db = db


def user(db):
    create_user(db=db)
    users = get_user_all(db=db)
    return users[0].id


def test_search_history_repository_sqlalchemy_add_should_persist_a_search_history(db):
    user_id = user(db=db)
    search_history_recovered = create_search_history(db=db, user_id=user_id)
    assert len(search_history_recovered) is 1


def test_search_history_repository_sqlalchemy_get_all_should_persist_list_search_history(db):
    user_id = user(db=db)
    for i in range(0, 3):
        create_search_history(db=db, user_id=user_id)

    search_history_recovered = get_search_history_all(db=db)
    assert type(search_history_recovered) is list
    assert len(search_history_recovered) is 3