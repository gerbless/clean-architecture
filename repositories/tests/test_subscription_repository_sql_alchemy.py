from repositories.tests.pytest_fixture_db import db
from repositories.tests.queries.queries_subscriptions import create_subscription, get_subscription_all
from repositories.tests.queries.queries_business_sector import create_business_sector
from repositories.tests.queries.queries_users import create_user, get_user_all
from repositories.tests.queries.queries_plans import create_plan

db = db


def user(db):
    create_user(db=db)
    users = get_user_all(db=db)
    return users[0].id


def test_subscription_repository_sqlalchemy_add_should_persist_a_subscription(db):
    user_id = user(db=db)
    business_sector = create_business_sector(db=db)
    business_sector_id = business_sector[0].id
    plan = create_plan(db=db)
    plan_id = plan[0].id
    subscription_recovered = create_subscription(db=db, user_id=user_id, business_sector_id=business_sector_id,
                                                 plan_id=plan_id)
    assert len(subscription_recovered) is 1


def test_subscription_repository_sqlalchemy_get_all_should_persist_list_subscription(db):
    user_id = user(db=db)
    business_sector = create_business_sector(db=db)
    business_sector_id = business_sector[0].id
    plan = create_plan(db=db)
    plan_id = plan[0].id
    for i in range(0, 3):
        create_subscription(db=db, user_id=user_id, business_sector_id=business_sector_id, plan_id=plan_id)

    subscription_recovered = get_subscription_all(db=db)
    assert type(subscription_recovered) is list
    assert len(subscription_recovered) is 3
