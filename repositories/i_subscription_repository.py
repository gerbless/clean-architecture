from abc import ABC, abstractmethod
from entities.subscription import Subscription


class ISubscriptionRepository(ABC):

    @abstractmethod
    def add(self, subscription: Subscription): pass

    @abstractmethod
    def get_all(self): pass
