from abc import ABC, abstractmethod
from entities.search_history import SearchHistory


class ISeachHistoryRepository(ABC):

    @abstractmethod
    def add(self, search_history: SearchHistory): pass

    @abstractmethod
    def get_all(self): pass