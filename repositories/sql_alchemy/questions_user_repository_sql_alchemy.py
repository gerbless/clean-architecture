from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from entities.questions_user import QuestionUser
from repositories.i_questions_user_repository import IQuestionsUserRepository
from use_cases.exceptions import InvalidQuestionsUserException


class QuestionUserRepositorySqlAlchemy(IQuestionsUserRepository):

    @inject
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def add(self, question_user: QuestionUser):
        try:
            self.db.session.add(question_user)
            self.db.session.commit()
        except IntegrityError:
            self.db.session.rollback()
            raise InvalidQuestionsUserException

    def get_all(self):
        return self.db.session.query(QuestionUser).all()

    def get_id(self, uid: int):
        question_user = self.db.session.query(QuestionUser).filter(QuestionUser.id == uid).first()
        if question_user:
            return question_user
        raise InvalidQuestionsUserException

    def delete(self, uid: int):
        try:
            self.db.session.query(QuestionUser).filter(QuestionUser.id == uid).delete()
            self.db.session.commit()
        except IntegrityError:
            self.db.session.rollback()
            raise InvalidQuestionsUserException
