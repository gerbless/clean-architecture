from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from entities.plan import Plan
from repositories.i_plan_repository import IPlanRepository
from use_cases.exceptions import InvalidPlanException


class PlanRepositorySqlAlchemy(IPlanRepository):

    @inject
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def add(self, plan: Plan):
        try:
            self.db.session.add(plan)
            self.db.session.commit()
        except IntegrityError:
            self.db.session.rollback()
            raise InvalidPlanException

    def get_all(self):
        return self.db.session.query(Plan).all()

    def get_id(self, uid: int):
        plan = self.db.session.query(Plan).get(uid)

        if plan:
            return plan

        raise InvalidPlanException
