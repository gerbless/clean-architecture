from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from entities.business_sector import BusinessSector
from repositories.i_business_sector_repository import IBusinessSectorRepository
from use_cases.exceptions import InvalidBusinessSectorException


class BusinessSectorRepositorySqlAlchemy(IBusinessSectorRepository):

    @inject
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def add(self, business_sector: BusinessSector):
        try:
            self.db.session.add(business_sector)
            self.db.session.commit()
        except IntegrityError:
            self.db.session.rollback()
            raise InvalidBusinessSectorException

    def get_all(self):
        return self.db.session.query(BusinessSector).all()

    def get_id(self, uid: int):
        question_user = self.db.session.query(BusinessSector).filter(BusinessSector.id == uid).first()
        if question_user:
            return question_user
        raise InvalidBusinessSectorException

    def get_qty_business_sectors_by_user(self, uid: int):
        return self.db.session.query(BusinessSector).filter(BusinessSector.id == uid).count()

    def delete(self, uid: int):
        try:
            self.db.session.query(BusinessSector).filter(BusinessSector.id == uid).delete()
            self.db.session.commit()
        except IntegrityError:
            self.db.session.rollback()
            raise InvalidBusinessSectorException
