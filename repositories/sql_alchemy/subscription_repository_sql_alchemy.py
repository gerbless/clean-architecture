from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from entities.subscription import Subscription
from repositories.i_subscription_repository import ISubscriptionRepository
from use_cases.exceptions import InvalidSubscriptionException


class SubscriptionRepositorySqlAlchemy(ISubscriptionRepository):

    @inject
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def add(self, subscription: Subscription):
        try:
            self.db.session.add(subscription)
            self.db.session.commit()
        except IntegrityError:
            self.db.session.rollback()
            raise InvalidSubscriptionException

    def get_all(self):
        return self.db.session.query(Subscription).all()
