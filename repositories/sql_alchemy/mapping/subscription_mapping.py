from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from entities.subscription import Subscription
from entities.plan import Plan


def subscription_mapping(metadata: MetaData):
    subscription = Table(
        'subscriptions',
        metadata,
        Column('id', Integer, Sequence('subscriptions_id_seq'), primary_key=True, nullable=False),
        Column('user_id', Integer, ForeignKey('users.id'), nullable=False),
        Column('business_sector_id', Integer, ForeignKey('business_sector.id')),
        Column('plan_id', Integer, ForeignKey('plans.id')),
        Column('subscriptionId', String(20)),
        Column('pay_method_id', Integer, default=0),
        Column('start', String(15)),
        Column('renewal', DateTime)
    )

    mapper(Subscription, subscription)

    return subscription
