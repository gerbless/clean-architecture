from sqlalchemy import Table, MetaData, Column, Integer, String, DateTime, ForeignKey, Sequence
from sqlalchemy.orm import mapper

from entities.search_history import SearchHistory


def search_history_mapping(metadata: MetaData):
    search_history = Table(
        'search_history',
        metadata,
        Column('id', Integer, Sequence('search_history_id_seq'), primary_key=True, nullable=False),
        Column('user_id', Integer, ForeignKey('users.id'), nullable=False),
        Column('search', String(150)),
        Column('history_date', DateTime)
    )

    mapper(SearchHistory, search_history)

    return search_history
