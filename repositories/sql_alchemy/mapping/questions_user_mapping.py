from sqlalchemy import Table, MetaData, Column, Integer, ForeignKey, Sequence
from sqlalchemy.orm import mapper

from entities.questions_user import QuestionUser


def questions_user_mapping(metadata: MetaData):
    questions_user = Table(
        'questions_user',
        metadata,
        Column('id', Integer, Sequence('questions_user_id_seq'), nullable=False, primary_key=True),
        Column('question_id', Integer, ForeignKey('users.id')),
        Column('user_id', Integer, ForeignKey('questions.id'))
    )

    mapper(QuestionUser, questions_user)

    return questions_user
