from sqlalchemy import Table, MetaData, Column, Integer, Text, ForeignKey, Sequence
from sqlalchemy.orm import mapper
from entities.question import Question


def preconfigured_questions_mapping(metadata: MetaData):
    preconfigured_questions = Table(
        'questions',
        metadata,
        Column('id', Integer, Sequence('questions_id_seq'), nullable=False, primary_key=True),
        Column('business_sector_id', Integer, ForeignKey('business_sector.id')),
        Column('question', Text),
        Column('description', Text)
    )

    mapper(Question, preconfigured_questions)

    return preconfigured_questions
