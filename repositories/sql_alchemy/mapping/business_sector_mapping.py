from sqlalchemy import Table, MetaData, Column, Integer, String, Sequence, Text
from sqlalchemy.orm import mapper, relationship

from entities.business_sector import BusinessSector
from entities.question import Question
from entities.subscription import Subscription


def business_sector_mapping(metadata: MetaData):
    business_sector = Table(
        'business_sector',
        metadata,
        Column('id', Integer, Sequence('business_sector_id_seq'), primary_key=True, nullable=False),
        Column('name_sector', String(50)),
        Column('type', String(50)),
        Column('description', Text)
    )

    mapper(BusinessSector, business_sector, properties={
        'preconfigured_question': relationship(Question, backref='business_sector', order_by=business_sector.c.id),
        'subscription': relationship(Subscription, backref='business_sector', order_by=business_sector.c.id)
    })

    return business_sector
