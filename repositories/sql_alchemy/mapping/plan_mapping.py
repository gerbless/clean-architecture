from sqlalchemy import Table, MetaData, Column, Integer, String, Sequence, Float, Text
from sqlalchemy.orm import mapper, relationship

from entities.plan import Plan
from entities.subscription import Subscription


def plan_mapping(metadata: MetaData):
    plan = Table(
        'plans',
        metadata,
        Column('id', Integer, Sequence('plans_id_seq'), primary_key=True, nullable=False),
        Column('name', String(80)),
        Column('amount', Float),
        Column('currency', String(50)),
        Column('interval', Integer),
        Column('description', Text)
    )

    mapper(Plan, plan, properties={
        'subscription': relationship(Subscription, backref='plan', order_by=plan.c.id)
    })

    return plan
