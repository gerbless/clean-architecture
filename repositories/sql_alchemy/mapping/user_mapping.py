from sqlalchemy import Table, MetaData, Column, Integer, String, LargeBinary, Sequence, Boolean
from sqlalchemy.orm import mapper, relationship

from entities.user import User
from entities.favorite_document import FavoriteDocument
from entities.search_history import SearchHistory
from entities.subscription import Subscription

def user_mapping(metadata: MetaData):
    user = Table(
        'users',
        metadata,
        Column('id', Integer, Sequence('users_id_seq'), nullable=False, primary_key=True),
        Column('first_name', String(50)),
        Column('last_name', String(50)),
        Column('email', String(50), unique=True),
        Column('encrypted_password', LargeBinary(60)),
        Column('confirm_email', Boolean),
        Column('customerId', String(20)),
        Column('external_uuid', String(60)),
        Column('credit_card', Boolean, default=False)
    )

    mapper(User, user, properties={
        'favorite_document': relationship(FavoriteDocument, backref='user', order_by=user.c.id),
        'search_history': relationship(SearchHistory, backref='user', order_by=user.c.id),
        'subscription': relationship(Subscription, backref='user')
    })
    return user
