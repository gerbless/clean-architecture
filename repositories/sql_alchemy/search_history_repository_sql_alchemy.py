from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from entities.search_history import SearchHistory
from repositories.i_search_history_repository import ISeachHistoryRepository
from use_cases.exceptions import InvalidSearchHistoryException


class SearchHistoryRepositorySqlAlchemy(ISeachHistoryRepository):

    @inject
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def add(self, search_history: SearchHistory):
        try:
            self.db.session.add(search_history)
            self.db.session.commit()
        except IntegrityError:
            self.db.session.rollback()
            raise InvalidSearchHistoryException

    def get_all(self):
        return self.db.session.query(SearchHistory).all()
