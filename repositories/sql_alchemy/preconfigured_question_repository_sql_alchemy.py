from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError, DataError, InvalidRequestError

from entities.question import Question
from repositories.i_preconfigured_questions_repository import IPreconfiguredQuestionsRepository
from use_cases.exceptions import InvalidQuestionsException


class PreconfiguredQuestionRepositorySqlAlchemy(IPreconfiguredQuestionsRepository):

    @inject
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def add(self, question: Question):
        try:
            self.db.session.add(question)
            self.db.session.commit()
        except IntegrityError as err:
            self.db.session.rollback()
            raise InvalidQuestionsException(message_user='Ocurrio un error al insertar los datos', err_debug=str(err))
        except DataError as err:
            raise InvalidQuestionsException(message_user='Ocurrio un error con los datos suministrados', err_debug=str(err))

        finally:
            self.db.session.close()

    def get_all(self):
        return self.db.session.query(Question).all()

    def get_id(self, uid: int):
        question = self.db.session.query(Question).filter(Question.id == uid).first()
        if question:
            return question
        raise InvalidQuestionsException

    def delete(self, uid: int):
        try:
            self.db.session.query(Question).filter(Question.id == uid).delete()
            self.db.session.commit()
        except IntegrityError as err:
            self.db.session.rollback()
            raise InvalidQuestionsException(message_user='Ocurrio un error al intenar eliminar los datos',
                                            err_debug=str(err))

    def to_update(self, uid: int, question: Question):
        try:
            self.db.session.query(Question).filter(Question.id == uid).update(question, synchronize_session=False)
            self.db.session.commit()
        except IntegrityError as err:
            self.db.session.rollback()
            raise InvalidQuestionsException(message_user='Ocurrio un error al intenar actualizar los datos',
                                            err_debug=str(err))
        except DataError as err:
            raise InvalidQuestionsException(message_user='Ocurrio un error con los datos suministrados', err_debug=str(err))

    def get_name_like(self, question: str):
        try:
            questions = self.db.session.query(Question).filter(Question.question.like('''%{}%'''.format(question))).all()
            print(questions)
            return questions
        except InvalidRequestError as err:
            raise InvalidQuestionsException(message_user='Ocurrio un error al intentar hacer la busqueda',
                                            err_debug=str(err))
