class FavoriteDocument:

    def __init__(self, title: str, author: str, description: str, publication_date: str, url: str,
                 host: str, score: float, user: int):
        self.title = title
        self.author = author
        self.description = description
        self.publication_date = publication_date
        self.url = url
        self.host = host
        self.score = score
        self.user_id = user
