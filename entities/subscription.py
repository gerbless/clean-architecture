from datetime import datetime


class Subscription:

    def __init__(self, user_id: int, business_sector_id: int, plan_id: int, subscriptionId: str, renewal: datetime = None):
        self.user_id = user_id
        self.business_sector_id = business_sector_id
        self.plan_id = plan_id
        self.subscriptionId = subscriptionId
        self.pay_method_id = 0
        self.start = self.date_now()
        self.renewal = renewal
        self.trial_period_days = 30

    def date_now(self):
        x = datetime.now()
        return "%s/%s/%s" % (x.day, x.month, x.year)


