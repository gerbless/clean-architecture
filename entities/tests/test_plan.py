from entities.plan import Plan


def test_plan_entity_init():
    plan = Plan("Plan junior", 20000.00, "CLP", 3, "Proin feugiat ipsum in diam finibus volutpat.")
    assert plan.name == "Plan junior"
    assert plan.amount == 20000
    assert plan.currency == "CLP"
    assert plan.interval == 3
    assert plan.description == "Proin feugiat ipsum in diam finibus volutpat."


def test_plan_entity_validate_type_data():
    plan = Plan("Plan junior", 20000.00, "CLP", 3, "Proin feugiat ipsum in diam finibus volutpat.")
    assert type(plan.name) is str
    assert type(plan.amount) is float
    assert type(plan.currency) is str
    assert type(plan.interval) is int
    assert type(plan.description) is str
