from entities.favorite_document import FavoriteDocument


def test_favorite_document_entity_init():
    favorite_document = FavoriteDocument("Top musical", "Martin Stein",
                                         "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                                         "14/01/18", "https://www.lipsum.com/xyz", "https://www.lipsum.com", 2.5, 5)

    assert favorite_document.title == "Top musical"
    assert favorite_document.author == "Martin Stein"
    assert favorite_document.description == "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    assert favorite_document.publication_date == "14/01/18"
    assert favorite_document.url == "https://www.lipsum.com/xyz"
    assert favorite_document.host == "https://www.lipsum.com"
    assert favorite_document.score == 2.5
    assert favorite_document.user_id == 5


def test_favorite_document_validate_type_data():
    favorite_document = FavoriteDocument("Top musical", "Martin Stein",
                                         "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                                         "14/01/18", "https://www.lipsum.com/xyz", "https://www.lipsum.com", 2.5, 5)

    assert type(favorite_document.title) is str
    assert type(favorite_document.author) is str
    assert type(favorite_document.description) is str
    assert type(favorite_document.publication_date) is str
    assert type(favorite_document.url) is str
    assert type(favorite_document.host) is str
    assert type(favorite_document.score) is float
    assert type(favorite_document.user_id) is int
