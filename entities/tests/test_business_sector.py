from entities.business_sector import BusinessSector


def test_business_sector_validate_that_the_question_is_not_empty():
    business_sector = BusinessSector("Legal", "Abogados", "Proin feugiat ipsum in diam finibus volutpat.")
    assert business_sector.type == "Abogados"
    assert business_sector.name_sector == "Legal"
    assert business_sector.description == "Proin feugiat ipsum in diam finibus volutpat."


def test_business_sector_validate_type_data():
    business_sector = BusinessSector("Legal", "Abogados", "Proin feugiat ipsum in diam finibus volutpat.")
    assert type(business_sector.type) is str
    assert type(business_sector.name_sector) is str
    assert type(business_sector.description) is str
