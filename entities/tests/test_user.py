import pytest

from entities.user import User


def test_user_creation_should_store_encrypted_password():
    user = User("Andres", "Orellana", "aorellana@mailinator.com", "password", False)
    with pytest.raises(AttributeError):
        user.password
    assert user.encrypted_password is not None
    assert user.encrypted_password != "password"


def test_user_validate_credentials_should_return_true_if_password_is_valid():
    user = User("Andres", "Orellana", "aorellana@mailinator.com", "password", True)
    assert user.valid_credential("password")


def test_user_validate_credentials_should_return_false_if_password_is_invalid():
    user = User("Andres", "Orellana", "aorellana@mailinator.com", "password", True)
    assert not user.valid_credential("password1")


def test_user_is_active_email_should_return_true_if_active_email():
    user = User("Andres", "Orellana", "aorellana@mailinator.com", "password", True)
    user.active_email()
    assert user.is_active_email() is True


def test_user_is_credit_card_should_return_true_if_register_credit_card():
    user = User("Andres", "Orellana", "aorellana@mailinator.com", "password", True)
    user.register_credit_card()
    assert user.is_credit_card() is True


def test_user_validate_type_data():
    user = User("Andres", "Orellana", "aorellana@mailinator.com", "password", False)
    assert type(user.first_name) == str
    assert type(user.last_name) == str
    assert type(user.email) == str
    assert type(user.encrypted_password) == bytes
    assert type(user.confirm_email) == bool
    assert type(user.external_uuid) == str
    assert type(user.credit_card) == bool
