from entities.search_history import SearchHistory
from datetime import datetime


def test_search_history_entity_init():
    history_date = datetime.now()
    search_history = SearchHistory("Top musical December", history_date, 5)
    assert search_history.search == "Top musical December"
    assert search_history.user_id == 5
    assert search_history.history_date == history_date


def test_search_history_validate_type_data():
    search_history = SearchHistory("Top musical December", datetime.now(), 5)
    assert type(search_history.user_id) is int
    assert type(search_history.search) is str
    assert type(search_history.history_date) is datetime
