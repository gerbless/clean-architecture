from datetime import datetime
from entities.subscription import Subscription


def test_subscription_entity_init():
    renewal = datetime.now()
    subscription = Subscription(user_id=1, business_sector_id=1, plan_id=1, subscriptionId="sus_azcyjj9ycd", renewal=renewal)
    assert subscription.user_id == 1
    assert subscription.business_sector_id == 1
    assert subscription.plan_id == 1
    assert subscription.subscriptionId == "sus_azcyjj9ycd"
    assert subscription.renewal == renewal


def test_subscription_entity_validate_type_data():
    subscription = Subscription(user_id=1, business_sector_id=1, plan_id=1, subscriptionId="sus_azcyjj9ycd", renewal=datetime.now())
    assert type(subscription.user_id) is int
    assert type(subscription.business_sector_id) is int
    assert type(subscription.plan_id) is int
    assert type(subscription.subscriptionId) is str
    assert type(subscription.renewal) is datetime
    assert type(subscription.start) is str
