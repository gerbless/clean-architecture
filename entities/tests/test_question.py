from entities.question import Question


def test_question_validate_that_the_question_is_not_empty():
    question = Question("this is a question", 10, "this is a description")
    assert question.business_sector_id == 10
    assert question.question == "this is a question"
    assert question.description == "this is a description"


def test_question_validate_type_data():
    question = Question("this is a question", 10, "this is a description")
    assert type(question.business_sector_id) == int
    assert type(question.question) == str
    assert type(question.description) == str
