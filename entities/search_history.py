from datetime import datetime


class SearchHistory:

    def __init__(self, search: str, history_date: datetime, user: int):
        self.search = search
        self.history_date = history_date
        self.user_id = user
