class Question:
    def __init__(self, question: str, business_sector_id: int, description: str = None, slug: str = None):
        self.question = question
        self.business_sector_id = business_sector_id
        self.description = description
        self.slug = slug

    def obj_to_update(self):
        question = {
            'question': self.question,
            'business_sector_id': self.business_sector_id,
            'description': self.description
        }
        return question

    def slug(self):
        pass
