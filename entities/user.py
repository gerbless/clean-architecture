import bcrypt
import uuid


class User:

    def __init__(self, first_name: str, last_name: str, email: str, password: str, confirm_email: bool):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.encrypted_password = bcrypt.hashpw(self.string_to_bit(password), bcrypt.gensalt())
        self.confirm_email = confirm_email
        self.customerId = None
        self.external_uuid = str(uuid.uuid4())
        self.credit_card = False

    def full_name(self):
        return self.first_name + " " + self.last_name

    def change_password(self, password):
        self.encrypted_password = bcrypt.hashpw(self.string_to_bit(password), bcrypt.gensalt())

    def valid_credential(self, password: str):
        return bcrypt.checkpw(self.string_to_bit(password), self.encrypted_password)

    def active_email(self):
        self.confirm_email = True

    def is_active_email(self):
        return self.confirm_email

    def update_customer(self, customer_id):
        self.customerId = customer_id

    def register_credit_card(self):
        self.credit_card = True

    def is_credit_card(self):
        return self.credit_card

    @staticmethod
    def string_to_bit(string):
        if type(string) is str:
            return string.encode('utf-8')
        return string
