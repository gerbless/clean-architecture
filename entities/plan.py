class Plan:

    def __init__(self, name: str, amount: float, currency: str, interval: int, description: str):
        self.name = name
        self.amount = amount
        self.currency = currency
        self.interval = interval
        self.description = description