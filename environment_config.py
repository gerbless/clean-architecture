import os
from abc import ABC

from dotenv import load_dotenv, find_dotenv


class IEnvironmentConfig(ABC):
    pass


class EnvironmentConfig(IEnvironmentConfig):
    load_dotenv(find_dotenv())

    MODE_DEBBUGER = os.environ.get('MODE_DEBBUGER', False)
    PORT = os.environ.get('PORT', 5000)
    TEMPLATE_DIR = os.environ.get('TEMPLATE_DIR', 'web/templates')
    SECRET_KEY = os.environ.get('SECRET_KEY', 'M#lOkNdmdAxaGS=GgEPl)&9_$JFNCE&djMPB30zwRwvMDQxFq&tTnv=)')

    MAILGUN = {
        "api_key": os.environ.get('MAILGUN_API_KEY'),
        "domain": os.environ.get('MAILGUN_DOMAIN')
    }

    FLOW = {
        "secret_key": os.environ.get('FLOW_SECRET_KEY').encode('utf-8'),
        "api_key": os.environ.get('FLOW_API_KEY')
    }

    DISCOVERY = {
        "username": os.environ.get('DISCOVERY_USERNAME'),
        "password": os.environ.get('DISCOVERY_PASSWORD'),
        "environment_id": os.environ.get('DISCOVERY_ENVIRONMENT_ID'),
        "collection_id": os.environ.get('DISCOVERY_COLLECTION_ID'),
        "version": os.environ.get('DISCOVERY_VERSION'),
    }

    DATABASE = os.environ.get('DATABASE')

    RECAPTCHA = {
        "site_secret": os.environ.get('SITE_SECRET'),
        "secret_key": os.environ.get('SECRET_KEY')
    }

    BASE_URL = os.environ.get('BASE_URL')
