jQuery('#slide-resultados').slippry({
    elements: 'article',
    pager: false,
    controls: false,
    auto: true,
    speed: 500,
    autoHover: false,
});

$('#panel-filtros').scotchPanel({
    containerSelector: 'body',
    direction: 'right',
    duration: 300,
    transition: 'ease',
    clickSelector: '.toggle-panel',
    distanceX: '300px',
    enableEscapeKey: true
});

$(function(){
    $('[data-toggle="tooltip"]').tooltip()
 });

// Formulario en pasos
jQuery(function($) {
    $('#suscripcion').wizard({
        onFinish: function() {
            $('#formulario').submit();
        },
        buttonsAppendTo: 'this',
            templates: {
                buttons: function() {
                    const options = this.options;
                    return `<div class="wizard-buttons d-flex justify-content-center">
                        <a class="wizard-back btn btn-primary" href="#${this.id}" data-wizard="back" role="button">${options.buttonLabels.back}</a>
                        <a class="wizard-next btn btn-primary" href="#${this.id}" data-wizard="next" role="button">${options.buttonLabels.next}</a>
                        <a class="wizard-finish btn btn-success" href="#${this.id}" data-wizard="finish" role="button">${options.buttonLabels.finish}</a></div>`;
            }
        },
        buttonLabels: {
            next: 'Siguiente',
            back: 'Volver',
            finish: 'Subscribirse por Flow'
        },
    });
});