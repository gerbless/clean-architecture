import json

import jinja2
from flask import Flask
from flask_injector import FlaskInjector
from flask_sqlalchemy import SQLAlchemy
from injector import Binder, singleton
from sqlalchemy import MetaData

from environment_config import EnvironmentConfig
from repositories.configuration import configure_repositories_binding
from repositories.sql_alchemy.mapping.business_sector_mapping import business_sector_mapping
from repositories.sql_alchemy.mapping.favorite_document_mapping import favorite_document_mapping
from repositories.sql_alchemy.mapping.plan_mapping import plan_mapping
from repositories.sql_alchemy.mapping.preconfigured_questions_mapping import preconfigured_questions_mapping
from repositories.sql_alchemy.mapping.questions_user_mapping import questions_user_mapping
from repositories.sql_alchemy.mapping.search_history_mapping import search_history_mapping
from repositories.sql_alchemy.mapping.subscription_mapping import subscription_mapping
from repositories.sql_alchemy.mapping.user_mapping import user_mapping
from seed import seed_plan, seed_business_sector
from services.configuration import configure_services_binding
from use_cases.configure_use_case_binding import configure_user_case_binding
from web.configuration import configure_web_route, configure_web_binding

templates_folders = [
    EnvironmentConfig.TEMPLATE_DIR,
]

ROUTING_MODULES = [
    configure_web_route
]


def configure_database_bindings(binder: Binder) -> Binder:
    application = binder.injector.get(Flask)
    metadata = MetaData()
    try:
        user_mapping(metadata)
        preconfigured_questions_mapping(metadata)
        business_sector_mapping(metadata)
        favorite_document_mapping(metadata)
        search_history_mapping(metadata)
        plan_mapping(metadata)
        subscription_mapping(metadata)
        pass
    except Exception:
        pass
    db = SQLAlchemy(application)

    metadata.reflect(db.engine)
    metadata.drop_all(db.engine)
    db.session.commit()

    metadata.create_all(db.engine)
    db.session.commit()

    with open('data.json', 'r') as json_data:
        data = json.load(json_data)
        for row in data["plans"]:
            seed_plan(db, row)
        for row in data["business_sector"]:
            seed_business_sector(db, row)

    binder.bind(SQLAlchemy, to=db, scope=singleton)
    return binder


modules_list = [
    configure_web_binding,
    configure_user_case_binding,
    configure_repositories_binding,
    configure_services_binding,
    configure_database_bindings
]


def create_app(templates_folders_list=templates_folders, modules=modules_list):
    application = Flask(__name__)

    application.config['SECRET_KEY'] = EnvironmentConfig.SECRET_KEY
    application.config['TESTING'] = True
    application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    application.config.update(
        SQLALCHEMY_DATABASE_URI=EnvironmentConfig.DATABASE
    )

    for routing in ROUTING_MODULES:
        routing(application)

    custom_loader = jinja2.ChoiceLoader([
        application.jinja_loader,
        jinja2.FileSystemLoader(templates_folders_list)
    ])

    FlaskInjector(app=application, modules=modules)
    application.jinja_loader = custom_loader

    return application


if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=EnvironmentConfig.PORT, debug=EnvironmentConfig.MODE_DEBBUGER)
