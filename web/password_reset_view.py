from flask import render_template, request, flash
from flask.views import MethodView
from injector import inject

from use_cases.exceptions import InvalidUserException
from use_cases.i_password_reset_use_case import IPasswordResetUseCase


class PasswordResetView(MethodView):

    @inject
    def __init__(self, password_reset_use_case: IPasswordResetUseCase):
        self.password_reset_use_case = password_reset_use_case

    def get(self):
        return render_template("password_reset.html")

    def post(self):
        email = request.form.get('email')
        try:
            template = render_template("messages/password_reset_email.html",
                                      password_reset_url=self.password_reset_use_case.generate_password_url(email))

            if self.password_reset_use_case.send_message(email, template):
                flash("Revise su bandeja de entrada para los próximos pasos.", "success")

            return render_template("password_reset.html", email=email)

        except InvalidUserException:
            flash("El correo electrónico no existe.", "error")
            return render_template("password_reset.html", email=email), 401
