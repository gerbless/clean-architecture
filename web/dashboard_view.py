from flask import render_template
from flask.views import MethodView
from injector import inject

from middleware.guardian_login import guardian_login
from middleware.check_subscription import check_subscription
from use_cases.i_navigation_lock_use_case import INavigationLockUseCase


class DashboardView(MethodView):

    @inject
    def __init__(self, navigation_lock_use_case: INavigationLockUseCase):
        self.navigation_lock_use_case = navigation_lock_use_case

    @guardian_login
    @check_subscription
    def get(self):
        return render_template("dashboard.html")
