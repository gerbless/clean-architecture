from flask import render_template, request, flash
from flask.views import MethodView
from injector import inject

from use_cases.exceptions import InvalidUserException
from use_cases.i_register_use_case import IRegisterUseCase


class RegisterView(MethodView):
    @inject
    def __init__(self, register_use_case: IRegisterUseCase):
        self.register_use_case = register_use_case

    def get(self):
        site_key = self.register_use_case.get_captcha_key()
        return render_template('register.html', sitekey=site_key)

    def post(self):
        name = request.form.get('name')
        last_name = request.form.get('lastname')
        email = request.form.get('email')
        password = request.form.get('password')
        password_confirmation = request.form.get('password_confirmation')
        captcha_response = request.form.get('g-recaptcha-response')
        site_key = self.register_use_case.get_captcha_key()

        if password == password_confirmation:

            if self.register_use_case.check_captcha(captcha_response):
                try:
                    user = self.register_use_case.register_user(
                        name=name,
                        last_name=last_name,
                        email=email,
                        password=password,
                        confirm_email=False)

                    template = render_template("messages/confirm_email.html",
                                               confirm_url=self.register_use_case.generate_verify_url(user.email))

                    if self.register_use_case.send_message(user, template):
                        return render_template("unconfirmed_account.html")

                except InvalidUserException:
                    flash("El usuario ya se encuentra registrado.", "error")
            else:
                flash("Inválido Captcha. Por favor, intente de nuevo.", "error")
        else:
            flash("Las contraseñas no coinciden", "error")

        return render_template("register.html", sitekey=site_key)
