from flask import render_template, session, request, flash
from flask.views import MethodView
from injector import inject

from middleware.guardian_login import guardian_login
from use_cases.exceptions import InvalidUserException
from use_cases.i_subscription_use_case import ISubscriptionUseCase


class SubscriptionView(MethodView):

    @inject
    def __init__(self, subscription_use_case: ISubscriptionUseCase):
        self.subscription_use_case = subscription_use_case

    @guardian_login
    def get(self):
        try:
            subscriptions = self.subscription_use_case.get_subscription_data(session.get('user'))
            return render_template("subscription.html", new_credit_card_url=subscriptions["new_credit_card_url"])
        except InvalidUserException:
            pass
        return b'error'

    @guardian_login
    def post(self):
        token = request.form.get('token')

        if token:
            status = self.subscription_use_case.get_card_register_status(token)
            if status:
                email = session.get('user')
                plan = session.get('plan')
                business_sector = session.get('business_sector')
                result = self.subscription_use_case.add_subscription(email, plan, business_sector)
                if result:
                    return render_template("finalized_subscription.html")
                else:
                    return render_template("finalized_subscription.html", error="error")
            else:
                return render_template("finalized_subscription.html", error="error")
