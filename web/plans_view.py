from flask import render_template, request, session, redirect, url_for
from flask.views import MethodView
from injector import inject
from use_cases.i_plan_use_case import IPlanUseCase
from middleware.guardian_login import guardian_login


class PlansView(MethodView):

    @inject
    def __init__(self, plan_use_case: IPlanUseCase):
        self.plan_use_case = plan_use_case

    @guardian_login
    def get(self):
        try:
            plans = self.plan_use_case.get_plans()
            return render_template("plans.html", plans=plans)
        except:
            pass
        return render_template("plans.html")

    @guardian_login
    def post(self):
        plan = request.form.get('plan')
        session['plan'] = plan
        return redirect(url_for('subscription'))
