from flask import render_template, request, flash, redirect, url_for
from flask.views import MethodView
from injector import inject

from middleware.guardian_login import guardian_login
from middleware.check_subscription import check_subscription
from use_cases.i_preconfigured_questions_case import IPreconfiguredQuestionsUseCase
from use_cases.i_business_sector_use_case import IBusinessSectorUseCase
from use_cases.i_navigation_lock_use_case import INavigationLockUseCase
from use_cases.exceptions import InvalidQuestionsException


class PreconfiguredQuestionView(MethodView):

    @inject
    def __init__(self, navigation_lock_use_case: INavigationLockUseCase,
                 preconfigured_question_use_case: IPreconfiguredQuestionsUseCase,
                 business_sector_use_case: IBusinessSectorUseCase):
        self.navigation_lock_use_case = navigation_lock_use_case
        self.preconfigured_question_use_case = preconfigured_question_use_case
        self.business_sector_use_case = business_sector_use_case

    @guardian_login
    def get(self, uid: int = None):
        url = request.path

        if url.find("delete") > 0:
            try:
                self.preconfigured_question_use_case.destroy_question(uid=uid)
                flash('Eliminado con exito.', 'success')
            except InvalidQuestionsException as err:
                flash(err, 'error')
            return redirect(url_for('question'))

        if url.find("create") > 0:
            list_business_sector = self.business_sector_use_case.index_business_sector()
            return render_template('preconfigured_question.html', list_businnes_sector=list_business_sector)

        if uid is None:
            try:
                questions = self.preconfigured_question_use_case.index_questions()
                return render_template('list_preconfigured_question.html', questions=questions)
            except InvalidQuestionsException:
                pass

        try:
            question = self.preconfigured_question_use_case.index_questions(uid=uid)
            list_business_sector = self.business_sector_use_case.index_business_sector()
            return render_template('preconfigured_question.html', question=question,
                                   list_businnes_sector=list_business_sector)
        except InvalidQuestionsException:
            pass

    @guardian_login
    def post(self, uid: int = None):
        question = request.form.get('question')
        business_sector = request.form.get('business_sector')
        description = request.form.get('description')
        url = request.path

        if str(question).strip() is "" or str(business_sector).strip() is "":
            flash('Debe completar todos los campos.', 'error')
            return render_template("preconfigured_question.html"), 400

        if uid is not None:
            try:
                self.preconfigured_question_use_case.update_questions(data=
                                                                      {'id': uid,
                                                                       'question': question,
                                                                       'business_sector': int(business_sector),
                                                                       'description': description})
                flash('Actualizado con exito.', 'success')
            except InvalidQuestionsException as err:
                flash(err, 'error')

        if url.find("create") > 0:
            try:
                self.preconfigured_question_use_case.store_question(data=
                                                                    {'question': question,
                                                                     'business_sector': int(business_sector),
                                                                     'description': description})
                flash('Creado con exito.', 'success')
            except InvalidQuestionsException as err:
                flash(err, 'error')
        return redirect(url_for('question'))
