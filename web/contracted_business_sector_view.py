from flask import render_template, redirect, url_for, session, request
from flask.views import MethodView
from injector import inject

from middleware.guardian_login import guardian_login
from middleware.check_subscription import check_subscription
from use_cases.i_business_sector_use_case import IBusinessSectorUseCase
from use_cases.i_navigation_lock_use_case import INavigationLockUseCase
from use_cases.exceptions import InvalidUserException


class ContractedBusinessSectorView(MethodView):

    @inject
    def __init__(self, navigation_lock_use_case: INavigationLockUseCase,
                 business_sector_use_case: IBusinessSectorUseCase):
        self.navigation_lock_use_case = navigation_lock_use_case
        self.business_sector_use_case = business_sector_use_case

    @guardian_login
    @check_subscription
    def get(self):
        try:
            business_sectors = self.business_sector_use_case.get_business_sectors_by_user(session['user'])
            if len(business_sectors) > 1:
                return render_template("contracted_business_sector.html", business_sectors=business_sectors)
            else:
                return redirect(url_for('search_documents'))
        except InvalidUserException:
            pass
        return b'error'

    @guardian_login
    @check_subscription
    def post(self):
        business_sector = request.form.get('business_sector')
        return redirect(url_for('search_documents'))
