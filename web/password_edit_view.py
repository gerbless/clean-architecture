from flask import render_template, request, flash
from flask.views import MethodView
from injector import inject
from use_cases.i_password_edit_use_case import IPasswordEditUseCase
from use_cases.exceptions import InvalidUserException


class PasswordEditView(MethodView):

    @inject
    def __init__(self, password_edit_use_case: IPasswordEditUseCase):
        self.password_edit_use_case = password_edit_use_case

    def get(self, token):
        email = self.password_edit_use_case.verify_token(token)

        if email:
            return render_template("password_edit.html", email=email)

        flash("El enlace para restablecer la contraseña no es válido o ha caducado.", "error")
        return render_template("password_edit.html"), 403

    def post(self, token):
        password = request.form.get('password')
        password_confirmation = request.form.get('password_confirmation')

        if password == password_confirmation:
            email = self.password_edit_use_case.verify_token(token)
            try:
                self.password_edit_use_case.update_password(email, password)
                flash("Tu contraseña ha sido cambiada", "success")
            except InvalidUserException:
                pass
        else:
            flash("Las contraseñas no coinciden", "error")

        return render_template("password_edit.html")