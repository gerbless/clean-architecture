from flask import render_template
from flask.views import MethodView
from injector import inject

from use_cases.i_navigation_lock_use_case import INavigationLockUseCase


class NavigationLockView(MethodView):

    @inject
    def __init__(self, navigation_lock_use_case: INavigationLockUseCase):
        self.navigation_lock_use_case = navigation_lock_use_case

    def get(self):
        return render_template("navigation_lock.html")
