import json

from flask import render_template, session, request
from flask.views import MethodView
from injector import inject

from middleware.guardian_login import guardian_login
from middleware.check_subscription import check_subscription
from use_cases.exceptions import InvalidDocumentException
from use_cases.i_search_document_use_case import ISearchDocumentUseCase
from use_cases.i_search_history_use_case import ISearchHistoryUseCase
from use_cases.i_navigation_lock_use_case import INavigationLockUseCase


class SearchDocumentsView(MethodView):

    @inject
    def __init__(self, navigation_lock_use_case: INavigationLockUseCase,
                 search_document_use_case: ISearchDocumentUseCase,
                 search_history_use_case: ISearchHistoryUseCase):
        self.navigation_lock_use_case = navigation_lock_use_case
        self.search_document_use_case = search_document_use_case
        self.search_history_use_case = search_history_use_case

    @guardian_login
    @check_subscription
    def get(self):
        search_history = self.search_history_use_case.get_search_history()
        return render_template("search_documents.html", search_history=search_history)

    @guardian_login
    @check_subscription
    def post(self):
        query = request.form.get('search')
        email = session.get('user')

        status = self.search_history_use_case.store_search_history(query, email)
        if status:
            try:
                results = self.search_document_use_case.get_documents_data(search_query=query, max_results=10,
                                                                           statistics=True)
                return render_template("search_documents.html", documents=results["results"],
                                       aggregations=json.dumps(results['aggregations']))
            except InvalidDocumentException:
                return b"Sin resultados"
        else:
            return b'Error al guardar el historial'
