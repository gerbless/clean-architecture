from flask import render_template, flash, redirect, url_for
from flask.views import MethodView
from injector import inject

from use_cases.exceptions import InvalidUserException
from use_cases.i_verify_account_use_case import IVerifyAccountUseCase
from use_cases.i_subscription_use_case import ISubscriptionUseCase

class VerifyAccountView(MethodView):

    @inject
    def __init__(self, verify_account_use_case: IVerifyAccountUseCase, subscription_use_case: ISubscriptionUseCase):
        self.verify_account_use_case = verify_account_use_case
        self.subscription_use_case = subscription_use_case

    def get(self, token):
        email = self.verify_account_use_case.verify_token(token)
        if email:
            confirmed_email = self.verify_account_use_case.is_confirmed_email(email)
            if confirmed_email:
                flash("La cuenta se encuentra confirmada. Por favor inicie sesión", "success")
                return redirect(url_for('login', step=2))
            else:
                try:
                    self.verify_account_use_case.confirmation_email(email)
                    self.subscription_use_case.add_customer(email)
                    return render_template('verify_account.html', email=email)
                except InvalidUserException:
                    pass

        flash("El enlace para verificar su correo no es válido o ha caducado.", "error")
        return render_template('verify_account.html'), 403