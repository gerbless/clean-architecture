from flask import Flask
from injector import Binder

from environment_config import IEnvironmentConfig, EnvironmentConfig
from web.hello_world_view import HelloWorldView
from web.register_view import RegisterView
from web.login_view import LoginView
from web.password_edit_view import PasswordEditView
from web.password_reset_view import PasswordResetView
from web.preconfigured_question_view import PreconfiguredQuestionView
from web.search_documents_view import SearchDocumentsView
from web.subscription_view import SubscriptionView
from web.dashboard_view import DashboardView
from web.search_question_view import SearchQuestionView
from web.verify_account_view import VerifyAccountView
from web.resend_verify_account_view import ResendVerifyAccountView
from web.navigation_lock_view import NavigationLockView
from web.business_sector_view import BusinessSectorView
from web.plans_view import PlansView
from web.contracted_business_sector_view import ContractedBusinessSectorView


def configure_web_route(app: Flask):
    app.add_url_rule('/', view_func=HelloWorldView.as_view("hello_world"))
    app.add_url_rule('/register', view_func=RegisterView.as_view("register"))
    app.add_url_rule('/subscribe/', view_func=SubscriptionView.as_view("subscription"))
    app.add_url_rule('/login', view_func=LoginView.as_view("login"))
    app.add_url_rule('/password_reset', view_func=PasswordResetView.as_view("password_reset"))
    app.add_url_rule('/password_edit/<token>', view_func=PasswordEditView.as_view("password_edit"))
    app.add_url_rule('/preconfigured_question/', view_func=PreconfiguredQuestionView.as_view('question'),
                     methods=['GET'])
    app.add_url_rule('/preconfigured_question/create', view_func=PreconfiguredQuestionView.as_view('create_question'),
                     methods=['GET', 'POST'])
    app.add_url_rule('/preconfigured_question/<int:uid>', view_func=PreconfiguredQuestionView.as_view('find_question'),
                     methods=['GET', 'POST'])
    app.add_url_rule('/preconfigured_question/delete/<int:uid>',
                     view_func=PreconfiguredQuestionView.as_view('delete_question'), methods=['GET'])
    app.add_url_rule('/search/', view_func=SearchDocumentsView.as_view("search_documents"))
    app.add_url_rule('/dashboard', view_func=DashboardView.as_view("dashboard"))
    app.add_url_rule('/search_question/', view_func=SearchQuestionView.as_view("search_question"))
    app.add_url_rule('/verify_account/<token>', view_func=VerifyAccountView.as_view("verify_account"))
    app.add_url_rule('/resend', view_func=ResendVerifyAccountView.as_view("resend"))
    app.add_url_rule('/lock', view_func=NavigationLockView.as_view("lock"))
    app.add_url_rule('/business_sectors', view_func=BusinessSectorView.as_view("sectors"))
    app.add_url_rule('/plans', view_func=PlansView.as_view("plans"))
    app.add_url_rule('/contracted_sectors/', view_func=ContractedBusinessSectorView.as_view("contracted_sectors"))


def configure_web_binding(binder: Binder):
    binder.bind(IEnvironmentConfig, EnvironmentConfig)
    return binder
