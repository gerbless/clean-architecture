from unittest.mock import MagicMock

import pytest
from injector import Binder

from app import create_app
from use_cases.i_plan_use_case import IPlanUseCase

plan_use_case: IPlanUseCase = MagicMock(IPlanUseCase)


def configure_bindings(binder: Binder) -> Binder:
    binder.bind(IPlanUseCase, to=plan_use_case)
    return binder


@pytest.fixture
def client():
    app = create_app(modules=[configure_bindings])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client


def test_business_sector_view_get_list_plans(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'

    plan_use_case.get_plans.return_value = [
        {'id': 1, 'name': 'Full', 'amount': '1000',
         'description': 'Proin feugiat ipsum in diam finibus volutpat.'},
        {'id': 2, 'name': 'Basico', 'amount': '500',
         'description': 'Proin feugiat ipsum in diam finibus volutpat.'}
    ]

    response = client.get("/plans")
    assert response.status_code == 200
    assert b'1' in response.data
    assert b'2' in response.data
    assert b'Full' in response.data
    assert b'Basico' in response.data
    assert b'1000' in response.data
    assert b'500' in response.data
    assert b'Proin feugiat ipsum in diam finibus volutpat.' in response.data


def test_business_sector_view_post_redirect_to_plans(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'

    response = client.post("/plans", data={"plan": 1})
    assert response.status_code == 302
    assert b'Redirecting...' in response.data
    assert b'/subscribe/' in response.data
