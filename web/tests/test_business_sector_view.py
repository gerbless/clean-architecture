from unittest.mock import MagicMock

import pytest
from injector import Binder

from app import create_app
from use_cases.i_business_sector_use_case import IBusinessSectorUseCase

business_sector_use_case: IBusinessSectorUseCase = MagicMock(IBusinessSectorUseCase)


def configure_bindings(binder: Binder) -> Binder:
    binder.bind(IBusinessSectorUseCase, to=business_sector_use_case)
    return binder


@pytest.fixture
def client():
    app = create_app(modules=[configure_bindings])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client


def test_business_sector_view_get_list_business_sectors(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'

    business_sector_use_case.get_business_sectors.return_value = [
        {'id': 1, 'name_sector': 'Legal', 'type': 'Abogados',
         'description': 'Proin feugiat ipsum in diam finibus volutpat.'},
        {'id': 2, 'name_sector': 'Educacion', 'type': 'Docentes',
         'description': 'Proin feugiat ipsum in diam finibus volutpat.'}
    ]

    response = client.get("/business_sectors")
    assert response.status_code == 200
    assert b'1' in response.data
    assert b'2' in response.data
    assert b'Legal' in response.data
    assert b'Educacion' in response.data
    assert b'Proin feugiat ipsum in diam finibus volutpat.' in response.data


def test_business_sector_view_post_redirect_to_plans(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'

    response = client.post("/business_sectors", data={"business_sector": 1})
    assert response.status_code == 302
    assert b'Redirecting...' in response.data
    assert b'/plans' in response.data
