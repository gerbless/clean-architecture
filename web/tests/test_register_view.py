import pytest

from unittest.mock import MagicMock
from injector import Binder
from app import create_app
from use_cases.i_register_use_case import IRegisterUseCase
from use_cases.exceptions import InvalidUserException

register_use_case: IRegisterUseCase = MagicMock(IRegisterUseCase)


def configure_bindings(binder: Binder) -> Binder:
    binder.bind(IRegisterUseCase, to=register_use_case)


@pytest.fixture
def client():
    app = create_app(modules=[configure_bindings])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client


def test_register_view_get_include_email_password_repassword(client):
    response = client.get("/register")
    assert response.status_code == 200
    assert b'name' in response.data
    assert b'lastname' in response.data
    assert b'email' in response.data
    assert b'password' in response.data
    assert b'password_confirmation' in response.data
    assert b'data-sitekey' in response.data
    assert b'submit' in response.data


def test_register_view_post_user_exist(client):
    register_use_case.register_user.side_effect = InvalidUserException()

    response = client.post("/register", data={
        'name': 'admin',
        'lastname': 'admin',
        'email': 'admin@gmail.com',
        'password': '123',
        'password_confirmation': '123',
        'g-recaptcha-response': '03ADlfD1_GpDZg0wmH6-oP-MDRILq3Lns-67xAA5SRfQ3RGGMqzy0z7SJgpkZWkO4S6Tqrgz1adWoZl44hD4KjEf81pEy7MjIGzusj7OgylwnJEXh3jbMxi3l4X95HZqQsXO3jQmb5uX9pl20WV6QJCU7gsOjHJ7BsNBYVtXEBBz1kADvZlbCwUAuERKl_IM6EcD14qjxOD_Mgh2Xm2R67POUB59K3P-4YTKmRIF3tBc93CFDGioMARjD-ksHWisGumgbcrxGXS3xsYrqCjicVAs1Rj9EZqF7sNx0aV3TwOYPBbKCu0gxozNcB-SgijWOMC1Ad0lkHwB1gx_ML_er3R4-QRaJL7ETSeA'
    })

    assert b'El usuario ya se encuentra registrado.' in response.data


def test_register_post_password_repeat_not_same(client):
    response = client.post("/register", data={
        'name': 'admin',
        'lastname': 'admin',
        'email': 'admin@gmail.com',
        'password': '123456',
        'password_confirmation': '123',
        'g-recaptcha-response': '03ADlfD1_GpDZg0wmH6-oP-MDRILq3Lns-67xAA5SRfQ3RGGMqzy0z7SJgpkZWkO4S6Tqrgz1adWoZl44hD4KjEf81pEy7MjIGzusj7OgylwnJEXh3jbMxi3l4X95HZqQsXO3jQmb5uX9pl20WV6QJCU7gsOjHJ7BsNBYVtXEBBz1kADvZlbCwUAuERKl_IM6EcD14qjxOD_Mgh2Xm2R67POUB59K3P-4YTKmRIF3tBc93CFDGioMARjD-ksHWisGumgbcrxGXS3xsYrqCjicVAs1Rj9EZqF7sNx0aV3TwOYPBbKCu0gxozNcB-SgijWOMC1Ad0lkHwB1gx_ML_er3R4-QRaJL7ETSeA'
    })

    assert bytes('Las contraseñas no coinciden', 'utf-8') in response.data


def test_register_post_register_user(client):
    register_use_case.check_captcha.return_value = True
    register_use_case.send_message.return_value = True

    response = client.post("/register", data={
        'name': 'admin',
        'lastname': 'admin',
        'email': 'admin@gmail.com',
        'password': '123456',
        'password_confirmation': '123456',
        'g-recaptcha-response': '03ADlfD1_GpDZg0wmH6-oP-MDRILq3Lns-67xAA5SRfQ3RGGMqzy0z7SJgpkZWkO4S6Tqrgz1adWoZl44hD4KjEf81pEy7MjIGzusj7OgylwnJEXh3jbMxi3l4X95HZqQsXO3jQmb5uX9pl20WV6QJCU7gsOjHJ7BsNBYVtXEBBz1kADvZlbCwUAuERKl_IM6EcD14qjxOD_Mgh2Xm2R67POUB59K3P-4YTKmRIF3tBc93CFDGioMARjD-ksHWisGumgbcrxGXS3xsYrqCjicVAs1Rj9EZqF7sNx0aV3TwOYPBbKCu0gxozNcB-SgijWOMC1Ad0lkHwB1gx_ML_er3R4-QRaJL7ETSeA'
    })

    register_use_case.register_user.assert_called_with(name="admin",
                                                       last_name="admin",
                                                       email="admin@gmail.com",
                                                       password="123456",
                                                       confirm_email=False)
    assert response.status_code == 200


def test_register_post_should_error_if_captcha_invalid(client):
    register_use_case.check_captcha.return_value = False

    response = client.post("/register", data={
        'name': 'admin',
        'lastname': 'admin',
        'email': 'admin@gmail.com',
        'password': '123456',
        'password_confirmation': '123456',
        'g-recaptcha-response': '03ADlfD1_GpDZg0wmH6-oP-MDRILq3Lns-67xAA5SRfQ3RGGMqzy0z7SJgpkZWkO4S6Tqrgz1adWoZl44hD4KjEf81pEy7MjIGzusj7OgylwnJEXh3jbMxi3l4X95HZqQsXO3jQmb5uX9pl20WV6QJCU7gsOjHJ7BsNBYVtXEBBz1kADvZlbCwUAuERKl_IM6EcD14qjxOD_Mgh2Xm2R67POUB59K3P-4YTKmRIF3tBc93CFDGioMARjD-ksHWisGumgbcrxGXS3xsYrqCjicVAs1Rj9EZqF7sNx0aV3TwOYPBbKCu0gxozNcB-SgijWOMC1Ad0lkHwB1gx_ML_er3R4-QRaJL7ETSeA'
    })

    assert bytes('Inválido Captcha. Por favor, intente de nuevo.', 'utf-8') in response.data
