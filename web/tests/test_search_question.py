import pytest

from app import create_app
from use_cases.search_question_use_case import ISearchQuestionUseCase
from use_cases.i_navigation_lock_use_case import INavigationLockUseCase
from unittest.mock import MagicMock
from injector import Binder

search_question_use_case: ISearchQuestionUseCase = MagicMock(ISearchQuestionUseCase)
navigation_lock_use_case: INavigationLockUseCase = MagicMock(INavigationLockUseCase)


def configure_bindings(binder: Binder) -> Binder:
    binder.bind(ISearchQuestionUseCase, to=search_question_use_case)
    binder.bind(INavigationLockUseCase, to=navigation_lock_use_case)


@pytest.fixture(scope="session")
def client():
    app = create_app(modules=[configure_bindings])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client


def test_search_question_view_get_include_fields_type_text_search(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    response = client.get("/search_question/")
    assert response.status_code is 200
    assert b'search' in response.data


def test_search_question_view_post_fails_empty_fields(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    response = client.post("/search_question/", data={'search': ''})
    assert response.status_code == 400
    assert b'Debe introducir una pregunta' in response.data


def test_search_question_view_posts_return_value(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    search_result: dict = [{
        "id": 1,
        "question": "My Question es una vida entera",
        "businnes_sector_id": 1,
        "description": "esto es una question de prueba"
    }]
    search_question_use_case.get_search_question.return_value = search_result
    response = client.post("/search_question/", data={'search': 'My Question'})
    use_case_result = search_question_use_case.get_search_question.assert_called_once_with(search='My Question')
    assert response.status_code == 200
    assert b'vida entera' in response.data
