import pytest

from unittest.mock import MagicMock
from app import create_app
from injector import Binder
from use_cases.i_password_edit_use_case import IPasswordEditUseCase

password_edit_use_case: IPasswordEditUseCase = MagicMock(IPasswordEditUseCase)


@pytest.fixture
def client():
    app = create_app(modules=[configure_bindings])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client


def test_password_edit_view_get_should_password_and_password_confirmation_field(client):
    response = client.get("/password_edit/Im1pcmFrZWwxNDAxQGdtYWlsLmNvbSI")
    assert response.status_code == 200
    assert b'password' in response.data
    assert b'password_confirmation' in response.data


def test_password_edit_view_get_should_error_if_invalid_token(client):
    password_edit_use_case.verify_token.return_value = None
    response = client.get("/password_edit/Im1pcmFrZWwxNDAxQGdtYWlsLmNvbSI")
    assert response.status_code == 403
    assert bytes("El enlace para restablecer la contraseña no es válido o ha caducado.", "utf-8") in response.data


def test_password_edit_view_post_password_password_and_confirmation_do_not_match(client):
    response = client.post("/password_edit/Im1pcmFrZWwxNDAxQGdtYWlsLmNvbSI",
                           data={'password': '123', 'password_confirmation': '1234'})
    assert bytes("Las contraseñas no coinciden", 'utf-8') in response.data


def test_password_edit_view_post_update_password_success(client):
    password_edit_use_case.update_password.return_value = True
    response = client.post("/password_edit/Im1pcmFrZWwxNDAxQGdtYWlsLmNvbSI",
                           data={'password': '1234', 'password_confirmation': '1234'})
    assert response.status_code == 200
    assert bytes("Tu contraseña ha sido cambiada", "utf-8") in response.data
    password_edit_use_case.update_password.return_value = MagicMock()


def configure_bindings(binder: Binder) -> Binder:
    binder.bind(IPasswordEditUseCase, to=password_edit_use_case)