from unittest.mock import MagicMock

import pytest
from injector import Binder

from app import create_app
from use_cases.exceptions import InvalidDocumentException
from use_cases.i_search_document_use_case import ISearchDocumentUseCase
from use_cases.i_search_history_use_case import ISearchHistoryUseCase
from use_cases.i_navigation_lock_use_case import INavigationLockUseCase

search_document_use_case: ISearchDocumentUseCase = MagicMock(ISearchDocumentUseCase)
search_history_use_case: ISearchHistoryUseCase = MagicMock(ISearchHistoryUseCase)
navigation_lock_use_case: INavigationLockUseCase = MagicMock(INavigationLockUseCase)


def configure_bindings(binder: Binder) -> Binder:
    binder.bind(ISearchDocumentUseCase, to=search_document_use_case)
    binder.bind(ISearchHistoryUseCase, to=search_history_use_case)
    binder.bind(INavigationLockUseCase, to=navigation_lock_use_case)
    return binder


@pytest.fixture
def client():
    app = create_app(modules=[configure_bindings])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client


def test_search_documents_view_get_include_search_field(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'

        response = client.get("/search/")
        assert response.status_code == 200
        assert b'search' in response.data
        assert b'submit' in response.data


def test_search_documents_view_post_should_return_list_of_documents_for_query_by_user_logged_in(client):
    search_document_use_case.get_documents_data.return_value = {
        "matching_results": 2315,
        "passages": [],
        "aggregations": [
            {
                "type": "term",
                "field": "enriched_text.sentiment.document.label",
                "results": [
                    {
                        "key": "positive",
                        "matching_results": 42996
                    },
                    {
                        "key": "negative",
                        "matching_results": 8420
                    },
                    {
                        "key": "neutral",
                        "matching_results": 787
                    }
                ]
            }
        ],
        "results": [
            {
                "id": "GoXBk6orGKTN7h",
                "text": "El top 10 del jueves 4 quedo asi",
                "publication_date": "2018-10-05T15:43:00Z",
                "title": "Lele Pons se prepara para lanzar otro sencillo",
                "result_metadata": {
                    "score": 20
                }
            }
        ]
    }

    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
        response = client.post("/search/", data={'query': 'top de musical'})
    assert response.status_code == 200
    assert b'Resultados encontrados' in response.data
    assert b'El top 10 del jueves 4 quedo asi' in response.data
    assert b'2018-10-05T15:43:00Z' in response.data
    assert b'Lele Pons se prepara para lanzar otro sencillo' in response.data
    assert b'20' in response.data


def test_search_documents_view_post_should_return_list_documents_empty_for_document_invalid_and_user_logged_in(client):
    search_document_use_case.get_documents_data.side_effect = InvalidDocumentException()

    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
        response = client.post("/search/", data={'query': ""})
    assert b'Sin resultados' in response.data


def test_search_documents_view_post_status_store_search_history_should_return_false_if_invalid_user_or_history(client):
    search_history_use_case.store_search_history.return_value = False

    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
        response = client.post("/search/", data={'query': 'top de musical'})
    assert b'Error al guardar el historial' in response.data
