from unittest.mock import MagicMock

import pytest
from injector import Binder

from app import create_app
from use_cases.exceptions import InvalidUserException
from use_cases.i_subscription_use_case import ISubscriptionUseCase

subscription_use_case: ISubscriptionUseCase = MagicMock(ISubscriptionUseCase)


def configure_bindings(binder: Binder) -> Binder:
    binder.bind(ISubscriptionUseCase, to=subscription_use_case)
    return binder


@pytest.fixture
def client():
    app = create_app(modules=[configure_bindings])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client


def test_subscription_view_get_should_return_301_code_if_not_logged_in(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = ''
    response = client.get("/subscribe")
    assert response.status_code == 301
    assert b'Redirecting...' in response.data


def test_subscription_view_get_should_return_301_code_if_email_of_user_do_not_exists(client):
    subscription_use_case.get_subscription_data.side_effect = InvalidUserException()
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'email@email.com'
    response = client.get("/subscribe")
    assert response.status_code == 301
    assert b'Redirecting...' in response.data
    subscription_use_case.get_subscription_data = MagicMock()


def test_subscription_view_post_card_register_status_return_success_if_token_valid(client):
    subscription_use_case.get_card_register_status.return_value = True

    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    response = client.post("/subscribe/", data={"token": "41097C28B5BD78C77F589FE4BC59E18AC333F"})

    assert bytes("Te has suscrito satisfactoriamente a Scoutwire", "utf-8") in response.data


def test_subscription_view_post_card_register_status_return_error_if_token_invalid(client):
    subscription_use_case.get_card_register_status.return_value = False

    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    response = client.post("/subscribe/", data={"token": "41097C28B5BD78C"})

    assert bytes("Hubo un error en el proceso de suscripción", "utf-8") in response.data


def test_subscription_view_post_add_subscription_return_success_subscription_valid(client):
    subscription_use_case.get_card_register_status.return_value = True
    subscription_use_case.add_subscription.return_value = True

    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    response = client.post("/subscribe/", data={"token": "41097C28B5BD78C77F589FE4BC59E18AC333F"})

    assert bytes("Te has suscrito satisfactoriamente a Scoutwire", "utf-8") in response.data


def test_subscription_view_post_add_subscription_return_error_subscription_invalid(client):
    subscription_use_case.get_card_register_status.return_value = True
    subscription_use_case.add_subscription.return_value = False

    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    response = client.post("/subscribe/", data={"token": "41097C28B5BD78C77F589FE4BC59E18AC333F"})

    assert bytes("Hubo un error en el proceso de suscripción", "utf-8") in response.data


def test_subscription_view_get_should_return_list_for_credit_cards_and_options_for_add_one_if_logged_in(client):
    subscription_use_case.get_subscription_data.return_value = {
        "credit_cards": [
            {
                "creditCardType": "Visa",
                "last4CardDigits": "4425"
            }
        ],
        "new_credit_card_url": "https://www.flow.cl/app/webpay/disclaimer.php?token=41097C28B5BD78C77F589FE4BC59E18AC333F9EU"
    }

    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    response = client.get("/subscribe/")

    assert response.status_code is 200
    assert bytes("Paso 4: Realiza el pago de la suscripción", "utf-8") in response.data
    assert b'https://www.flow.cl/app/webpay/disclaimer.php?token=41097C28B5BD78C77F589FE4BC59E18AC333F9EU' in response.data
    subscription_use_case.get_subscription_data = MagicMock()
