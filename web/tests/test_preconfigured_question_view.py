import pytest

from app import create_app
from use_cases.i_preconfigured_questions_case import IPreconfiguredQuestionsUseCase
from use_cases.i_business_sector_use_case import IBusinessSectorUseCase
from use_cases.i_navigation_lock_use_case import INavigationLockUseCase
from unittest.mock import MagicMock
from injector import Binder

preconfigured_question_use_case: IPreconfiguredQuestionsUseCase = MagicMock(IPreconfiguredQuestionsUseCase)
business_sector_use_case: IBusinessSectorUseCase = MagicMock(IBusinessSectorUseCase)
navigation_lock_use_case: INavigationLockUseCase = MagicMock(INavigationLockUseCase)


def configure_bindings(binder: Binder) -> Binder:
    binder.bind(IPreconfiguredQuestionsUseCase, to=preconfigured_question_use_case)
    binder.bind(IBusinessSectorUseCase, to=business_sector_use_case)
    binder.bind(INavigationLockUseCase, to=navigation_lock_use_case)


@pytest.fixture
def client():
    app = create_app(modules=[configure_bindings])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client

    preconfigured_question_use_case.store_question = MagicMock()
    preconfigured_question_use_case.destroy_question = MagicMock()
    preconfigured_question_use_case.index_questions = MagicMock()
    preconfigured_question_use_case.update_questions = MagicMock()


def test_preconfigured_question_view_get_include_the_fields_question_business_sector_description(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    response = client.get("/preconfigured_question/create")
    assert response.status_code is 200
    assert b'question' in response.data
    assert b'business_sector' in response.data
    assert b'description' in response.data


def test_preconfigured_question_view_get_list_show_question_preconfigured_by_id(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    preconfigured_question_use_case.index_questions.return_value = {'id': 1,
                                                                    'question': 'question 1',
                                                                    'business_sector': 1,
                                                                    'description': 'sector legal (abogados)'}

    response = client.get("/preconfigured_question/1")
    preconfigured_question_use_case.index_questions.assert_called_once_with(uid=1)
    business_sector_use_case.index_business_sector()
    assert response.status_code is 200
    assert b'1' in response.data
    assert b'question 1' in response.data
    assert b'sector legal (abogados)' in response.data


def test_preconfigured_question_view_get_list_question_preconfigured(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    preconfigured_question_use_case.index_questions.return_value = [
        {'id': 1, 'question': 'question 1', 'business_sector': 1, 'description': 'sector legal (abogados)'},
        {'id': 2, 'question': 'question 2', 'business_sector': 2, 'description': 'sector tecnologico'},
        {'id': 3, 'question': 'question 3', 'business_sector': 3, 'description': 'sector undustrial'},
    ]
    response = client.get("/preconfigured_question/")
    preconfigured_question_use_case.index_questions.assert_called_once_with()
    assert response.status_code is 200
    assert b'question 1' in response.data
    assert b'question 2' in response.data
    assert b'question 3' in response.data
    assert b'1' in response.data
    assert b'2' in response.data
    assert b'3' in response.data
    assert b'sector legal (abogados)' in response.data
    assert b'sector tecnologico' in response.data
    assert b'sector undustrial' in response.data


def test_preconfigured_question_view_post_preconfigured_questions_fails_empty_fields(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    response = client.post("/preconfigured_question/create", data={
        'question': '',
        'business_sector': '',
        'description': ' '
    })
    assert response.status_code == 400
    assert b'Debe completar todos los campos' in response.data


def test_preconfigured_question_view_post_preconfigured_questions_add(client):
    preconfigured_question_use_case.store_question.return_value = True
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    response = client.post("/preconfigured_question/create", data={
        'question': 'Que opina usted de smartbrid',
        'business_sector': 1,
        'description': 'sector legal (abogados)'
    })
    preconfigured_question_use_case.store_question.assert_called_once_with(data={
        'question': 'Que opina usted de smartbrid',
        'business_sector': 1,
        'description': 'sector legal (abogados)'
    })
    assert response.status_code == 302
    assert b'/preconfigured_question' in response.data


def test_preconfigured_question_view_post_preconfigured_questions_update(client):
    preconfigured_question_use_case.update_questions.return_value = True
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    response = client.post("/preconfigured_question/1", data={
        'id': 1,
        'question': 'Que opina usted de smartbrid',
        'business_sector': 1,
        'description': 'sector legal (abogados)'
    })
    preconfigured_question_use_case.update_questions.assert_called_once_with(data={
        'id': 1,
        'question': 'Que opina usted de smartbrid',
        'business_sector': 1,
        'description': 'sector legal (abogados)'
    })
    assert response.status_code == 302
    assert b'/preconfigured_question/' in response.data


def test_preconfigured_question_view_get_question_preconfigured_delete(client):
    preconfigured_question_use_case.destroy_question.return_value = True
    with client as c:
        with c.session_transaction() as sess:
            sess['user'] = 'admin@gmail.com'
    response = client.get("/preconfigured_question/delete/1", data={
        'action': 'delete',
        'id': '1'
    })
    preconfigured_question_use_case.destroy_question.assert_called_once_with(uid=1)
    assert response.status_code == 302
    assert b'/preconfigured_question/' in response.data
