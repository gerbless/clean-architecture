import pytest

from unittest.mock import MagicMock
from injector import Binder
from app import create_app
from use_cases.i_verify_account_use_case import IVerifyAccountUseCase
from use_cases.i_subscription_use_case import ISubscriptionUseCase
from use_cases.subscription_use_case import SubscriptionUseCase

verify_account_use_case: IVerifyAccountUseCase = MagicMock(IVerifyAccountUseCase)
subscription_use_case: SubscriptionUseCase = MagicMock(SubscriptionUseCase)


def configure_bindings(binder: Binder) -> Binder:
    binder.bind(IVerifyAccountUseCase, to=verify_account_use_case)
    binder.bind(ISubscriptionUseCase, to=subscription_use_case)


@pytest.fixture
def client():
    app = create_app(modules=[configure_bindings])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client


def test_verify_account_view_get_should_error_if_invalid_token(client):
    verify_account_use_case.verify_token.return_value = None
    response = client.get("/verify_account/Im1pcmFrZWwxNDAxQGdtYWlsLmNvbSI")
    assert response.status_code == 403
    assert bytes("El enlace para verificar su correo no es válido o ha caducado.", "utf-8") in response.data


def test_verify_account_view_get_should_true_if_confirmed_email(client):
    verify_account_use_case.verify_token.return_value = True
    verify_account_use_case.is_confirmed_email.return_value = True
    response = client.get("/verify_account/Im1pcmFrZWwxNDAxQGdtYWlsLmNvbSI")
    assert response.status_code == 302
    assert b'Redirecting...' in response.data
    assert b'/login' in response.data


def test_verify_account_view_get_should_success_if_confirmation_email_is_true(client):
    verify_account_use_case.verify_token.return_value = True
    verify_account_use_case.is_confirmed_email.return_value = False
    verify_account_use_case.confirmation_email.return_value = True
    response = client.get("/verify_account/Im1pcmFrZWwxNDAxQGdtYWlsLmNvbSI")
    assert bytes("Verificación confirmada", "utf-8") in response.data
