import pytest

from unittest.mock import MagicMock
from app import create_app
from injector import Binder
from use_cases.exceptions import InvalidUserException
from use_cases.i_resend_verify_account_use_case import IResendVerifyAccountUseCase

resend_verify_account_use_case: IResendVerifyAccountUseCase = MagicMock(IResendVerifyAccountUseCase)


@pytest.fixture
def client():
    app = create_app(modules=[configure_bindings])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client


def test_resend_verify_account_view_get_should_email_field(client):
    response = client.get("/resend")
    assert response.status_code == 200
    assert b'email' in response.data
    assert b'submit' in response.data


def test_resend_verify_account_view_post_non_registered_email(client):
    resend_verify_account_use_case.send_message.side_effect = InvalidUserException()
    response = client.post("/resend", data={'email': 'adm@gmail.com'})
    assert response.status_code == 401
    assert bytes("El correo electrónico no existe.", "utf-8") in response.data
    resend_verify_account_use_case.send_message = MagicMock()


def test_resend_verify_account_view_post_registered_email(client):
    resend_verify_account_use_case.send_message.return_value = True
    response = client.post("/resend", data={'email': 'admin@gmail.com'})
    assert response.status_code == 200
    assert bytes("Revise su bandeja de entrada para confirmar su cuenta.", "utf-8") in response.data


def configure_bindings(binder: Binder) -> Binder:
    binder.bind(IResendVerifyAccountUseCase, to=resend_verify_account_use_case)