import pytest

from unittest.mock import MagicMock
from app import create_app
from injector import Binder
from use_cases.i_login_use_case import ILoginUseCase
from use_cases.i_verify_account_use_case import IVerifyAccountUseCase

login_use_case: ILoginUseCase = MagicMock(ILoginUseCase)
verify_account_use_case: IVerifyAccountUseCase = MagicMock(IVerifyAccountUseCase)


@pytest.fixture
def client():
    app = create_app(modules=[configure_bindings])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client


def test_login_view_get_include_email_password_field(client):
    response = client.get("/login")
    assert response.status_code == 200
    assert b'email' in response.data
    assert b'password' in response.data
    assert b'submit' in response.data


def test_login_view_post_invalid_email_password(client):
    login_use_case.valid_credential.return_value = False
    response = client.post("/login", data={'email': 'admin@gmail.com', 'password': '123'})
    assert response.status_code == 401
    assert bytes('Inválido correo electrónico o contraseña, por favor intente de nuevo.', 'utf-8') in response.data


def test_login_view_post_valid_email_password_confirmed_email(client):
    verify_account_use_case.is_confirmed_email.return_value = True
    login_use_case.valid_credential.return_value = True
    response = client.post("/login", data={'email': 'admin@gmail.com', 'password': '123456'})
    assert response.status_code == 302
    assert b'Redirecting...' in response.data
    assert b'/contracted_sectors/' in response.data


def test_login_view_post_valid_email_password_unconfirmed_email(client):
    verify_account_use_case.is_confirmed_email.return_value = False
    login_use_case.valid_credential.return_value = True
    response = client.post("/login", data={'email': 'admin@gmail.com', 'password': '123456'})
    assert response.status_code == 200
    assert b'No has confirmado tu cuenta' in response.data


def test_login_view_post_unconfirmed_email(client):
    verify_account_use_case.is_confirmed_email.return_value = False
    response = client.post("/login", data={'email': 'admin@gmail.com', 'password': '123456'})
    assert response.status_code == 200
    assert b'No has confirmado tu cuenta' in response.data


def configure_bindings(binder: Binder) -> Binder:
    binder.bind(ILoginUseCase, to=login_use_case)
    binder.bind(IVerifyAccountUseCase, to=verify_account_use_case)
