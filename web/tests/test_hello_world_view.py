import pytest

from app import create_app


@pytest.fixture
def client():
    app = create_app(modules=[])
    app.config['TESTING'] = True
    app.secret_key = "some_secret"
    test_client = app.test_client()

    yield test_client

def test_hello_world_view(client):
    response = client.get("/")
    assert response.status_code is 200
    assert b'Hello World' in response.data