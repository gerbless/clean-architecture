from flask import render_template, request, redirect, url_for, session
from flask.views import MethodView
from injector import inject
from use_cases.i_business_sector_use_case import IBusinessSectorUseCase
from middleware.guardian_login import guardian_login


class BusinessSectorView(MethodView):

    @inject
    def __init__(self, business_sector_use_case: IBusinessSectorUseCase):
        self.business_sector_use_case = business_sector_use_case

    @guardian_login
    def get(self):
        try:
            business_sectors = self.business_sector_use_case.get_business_sectors()
            return render_template("business_sectors.html", business_sectors=business_sectors)
        except:
            pass
        return render_template("business_sectors.html")

    @guardian_login
    def post(self):
        business_sector = request.form.get('business_sector')
        session['business_sector'] = business_sector
        return redirect(url_for('plans'))
