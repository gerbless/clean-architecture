from flask import render_template, request, flash
from flask.views import MethodView
from injector import inject

from use_cases.exceptions import InvalidUserException
from use_cases.i_resend_verify_account_use_case import IResendVerifyAccountUseCase


class ResendVerifyAccountView(MethodView):

    @inject
    def __init__(self, resend_verify_account_use_case: IResendVerifyAccountUseCase):
        self.resend_verify_account_use_case = resend_verify_account_use_case

    def get(self):
        return render_template("resend_verify_account.html")

    def post(self):
        email = request.form.get('email')
        try:
            template = render_template("messages/confirm_email.html",
                                       confirm_url=self.resend_verify_account_use_case.generate_verify_url(email))

            if self.resend_verify_account_use_case.send_message(email, template):
                flash("Revise su bandeja de entrada para confirmar su cuenta.", "success")
            return render_template("resend_verify_account.html", email=email)

        except InvalidUserException:
            flash("El correo electrónico no existe.", "error")
            return render_template("resend_verify_account.html", email=email), 401
