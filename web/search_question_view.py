from flask import render_template, request, flash
from flask.views import MethodView
from injector import inject

from middleware.guardian_login import guardian_login
from middleware.check_subscription import check_subscription
from use_cases.exceptions import InvalidSearchQuestionException
from use_cases.i_search_question_use_case import ISearchQuestionUseCase
from use_cases.i_navigation_lock_use_case import INavigationLockUseCase


class SearchQuestionView(MethodView):

    @inject
    def __init__(self, navigation_lock_use_case: INavigationLockUseCase,
                 search_question_use_case: ISearchQuestionUseCase):
        self.navigation_lock_use_case = navigation_lock_use_case
        self.search_question_use_case = search_question_use_case

    @guardian_login
    #@check_subscription
    def get(self):
        return render_template(template_name_or_list='search_question.html')

    @guardian_login
    #@check_subscription
    def post(self):
        search = request.form.get('search')
        if str(search).strip() is "":
            flash('Debe introducir una pregunta', 'error')
            return render_template(template_name_or_list='search_question.html'), 400
        try:
            search_question = self.search_question_use_case.get_search_question(search=search)
            if len(search_question) is 0:
                flash('No podemos responderte esta pregunta por ahora', 'success')

            return render_template(template_name_or_list="search_question.html", search_question=search_question)
        except InvalidSearchQuestionException as err:
            flash(err, 'error')

        return render_template(template_name_or_list="search_question.html")
