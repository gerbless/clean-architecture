from flask import render_template, request, flash, redirect, url_for, session
from flask.views import MethodView
from injector import inject

from use_cases.i_login_use_case import ILoginUseCase
from use_cases.i_verify_account_use_case import IVerifyAccountUseCase
from use_cases.exceptions import InvalidUserException


class LoginView(MethodView):

    @inject
    def __init__(self, login_use_case: ILoginUseCase, verify_account_use_case: IVerifyAccountUseCase):
        self.login_use_case = login_use_case
        self.verify_account_use_case = verify_account_use_case

    def get(self):
        session.clear()
        return render_template("login.html")

    def post(self):
        email = request.form.get('email')
        password = request.form.get('password')
        step = request.args.get('step')

        try:
            confirmed_email = self.verify_account_use_case.is_confirmed_email(email)
            if confirmed_email:
                valid_credential = self.login_use_case.valid_credential(email=email, password=password)

                if valid_credential:
                    session['user'] = email
                    flash('Has iniciado sesión correctamente', 'success')

                    if step:
                        return redirect(url_for('sectors'))
                    else:
                        return redirect(url_for('contracted_sectors'))
                else:
                    flash('Inválido correo electrónico o contraseña, por favor intente de nuevo.', 'error')
                    return render_template("login.html"), 401
        except InvalidUserException:
            flash('Inválido correo electrónico o contraseña, por favor intente de nuevo.', 'error')
            return render_template("login.html"), 401

        return render_template("unconfirmed_account.html")
