from entities.plan import Plan
from entities.business_sector import BusinessSector
from repositories.sql_alchemy.plan_repository_sql_alchemy import PlanRepositorySqlAlchemy
from repositories.sql_alchemy.business_sector_repository_sql_alchemy import BusinessSectorRepositorySqlAlchemy
from use_cases.exceptions import InvalidPlanException, InvalidBusinessSectorException


def seed_plan(db, data):
    plan = Plan(data["name"], data["amount"], data["currency"], data["interval"], data["description"])
    try:
        plan_repository = PlanRepositorySqlAlchemy(db)
        plan_repository.add(plan)
    except InvalidPlanException:
        raise InvalidPlanException()


def seed_business_sector(db, data):
    business_sector = BusinessSector(data["name_sector"], data["type"], data["description"])
    try:
        business_sector_repository = BusinessSectorRepositorySqlAlchemy(db)
        business_sector_repository.add(business_sector)
    except InvalidBusinessSectorException:
        raise InvalidBusinessSectorException()
